package com.auca.ranga.controller;

import com.auca.ranga.utility.SMS;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("notifications")
public class NotificationController {

    @PostMapping(value = "/sms")
    public ResponseEntity<Object> sendSms(@RequestBody SMSObject so){
        return new ResponseEntity<>(SMS.sendSingleSMS(so.number, so.sms), HttpStatus.OK);
    }

  public static class SMSObject{
        private String sms;
        private String number;

      public String getSms() {
          return sms;
      }

      public void setSms(String sms) {
          this.sms = sms;
      }

      public String getNumber() {
          return number;
      }

      public void setNumber(String number) {
          this.number = number;
      }
  }


}
