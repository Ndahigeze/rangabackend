package com.auca.ranga.controller;

import com.auca.ranga.dao.SubscriptionDao;
import com.auca.ranga.domain.SubCategory;
import com.auca.ranga.domain.Subscription;
import com.auca.ranga.utility.Messages;
import com.auca.ranga.utility.ResponseBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("subscription")
public class SubscriptionController {
    @Autowired
    private SubscriptionDao dao;

    @PostMapping(value = "/save",consumes = MediaType.APPLICATION_JSON_VALUE,produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> save(@RequestBody InnerSub sc){
        ResponseBean rs=new ResponseBean();

        try{
           Subscription sub=dao.findByCategory(SubCategory.valueOf(sc.getCategory()));
           if(sub!=null){
               sub.setAmount(sc.amount);
               sub.setMonths(sc.months);
               dao.save(sub);
           }else{
               Subscription sub2=new Subscription();
               sub2.setMonths(sc.months);
               sub2.setAmount(sc.amount);
               sub2.setCategory(SubCategory.valueOf(sc.category));
               dao.save(sub);
           }

            rs.setCode(Messages.ERROR_CODE);
            rs.setDescription("SUBSCRIPTION CATEGORY SAVED");

        }catch (Exception ex){
            rs.setCode(Messages.ERROR_CODE);
            rs.setDescription("Error occured try again");
            ex.printStackTrace();
        }
       return new ResponseEntity<Object>(rs,HttpStatus.OK);
    }


    @PutMapping(value = "/update/{uuid}",consumes = MediaType.APPLICATION_JSON_VALUE,produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> update(@RequestBody InnerSub sc,@PathVariable String uuid){
        ResponseBean rs=new ResponseBean();

        try{
            Subscription sub=dao.findByUuid(uuid);
            if(sub!=null){
                sub.setAmount(sc.amount);
                sub.setMonths(sc.months);
                dao.save(sub);
                rs.setCode(Messages.SUCCESS_CODE);
                rs.setDescription("SUBSCRIPTION CATEGORY UPDATED");
            }else{
                rs.setCode(Messages.ERROR_CODE);
                rs.setDescription("SUBSCRIPTION CATEGORY NOT UPDATED");
            }
        }catch (Exception ex){
            rs.setCode(Messages.ERROR_CODE);
            rs.setDescription("Error occured try again");
            ex.printStackTrace();
        }
        return new ResponseEntity<Object>(rs,HttpStatus.OK);
    }





    @GetMapping(value = "",produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> findALL(){
        ResponseBean rs=new ResponseBean();
        try {
              List<Subscription> list=dao.findAll();
              rs.setCode(Messages.SUCCESS_CODE);
              rs.setObject(list);

        }catch (Exception ex){
                  rs.setCode(Messages.ERROR_CODE);
                  rs.setDescription("Error Occured Try Again");
                  ex.printStackTrace();
        }
        return new ResponseEntity<>(rs,HttpStatus.OK);
    }

    @GetMapping(value = "/{uuid}",produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> findByuuid(@PathVariable String uuid){
        ResponseBean rs=new ResponseBean();
        try {
            Subscription cs=dao.findByUuid(uuid);
            rs.setObject(cs);
            rs.setCode(Messages.SUCCESS_CODE);

        }catch (Exception ex){
            rs.setCode(Messages.ERROR_CODE);
            rs.setDescription("Error Occured Try Again");
            ex.printStackTrace();
        }
        return new ResponseEntity<>(rs,HttpStatus.OK);
    }

    @GetMapping(value = "/categories",produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> findCategory(){
        ResponseBean rs=new ResponseBean();
        try{
            Subscription low=dao.findByCategory(SubCategory.valueOf("LOW"));
            Subscription medium=dao.findByCategory(SubCategory.valueOf("MEDIUM"));
            Subscription high=dao.findByCategory(SubCategory.valueOf("HIGH"));
            Map<String, Object> map=new HashMap<String,Object>(){{
                put("low",low);
                put("medium",medium);
                put("high",high);
            }};
            rs.setObject(map);
            rs.setCode(Messages.SUCCESS_CODE);

        }catch (Exception ex){
            rs.setDescription("Error Ocured try again");
            rs.setCode(Messages.ERROR_CODE);
        }
        return new ResponseEntity<>(rs,HttpStatus.OK);
    }


    public static class InnerSub{
        private int months;
        private String category;
        private double amount;

        public int getMonths() {
            return months;
        }

        public void setMonths(int months) {
            this.months = months;
        }

        public String getCategory() {
            return category;
        }

        public void setCategory(String category) {
            this.category = category;
        }

        public double getAmount() {
            return amount;
        }

        public void setAmount(double amount) {
            this.amount = amount;
        }
    }

}
