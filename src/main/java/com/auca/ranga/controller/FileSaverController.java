package com.auca.ranga.controller;

import com.auca.ranga.domain.FileSaver;
import com.auca.ranga.service.FileSaverService;
import com.auca.ranga.service.PropertyService;
import com.auca.ranga.utility.Messages;
import com.auca.ranga.utility.ResponseBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("filesavers")
public class FileSaverController {

    @Autowired
    private FileSaverService fileSaverService;

    @Autowired
    private PropertyService propertyService;

    @RequestMapping(value = "/save", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> create(@RequestBody FileSaver fileSaver, HttpServletRequest request) {
        ResponseBean responseBean = new ResponseBean();
        try {
            String token=request.getHeader(Messages.token_name);
            String doneBy=request.getHeader("doneBy");
            if(token.equalsIgnoreCase(Messages.token)){
                    fileSaver.setDoneBy(doneBy);
                    fileSaver.setLastUpdatedBy(doneBy);

                    int status=fileSaverService.createOrUpdate(fileSaver);
                    if(status==Messages.SUCCESS_CODE){
                        responseBean.setCode(Messages.SUCCESS_CODE);
                        responseBean.setDescription(Messages.save);
                        responseBean.setObject(fileSaver);
                    }else{
                        responseBean.setCode(Messages.ERROR_CODE);
                        responseBean.setDescription(Messages.error);
                        responseBean.setObject(fileSaver);
                    }

            }else{
                responseBean.setCode(Messages.ERROR_CODE);
                responseBean.setDescription(Messages.incorrect_token);
            }

        }catch (Exception ex) {

        }
        return new ResponseEntity<Object>(responseBean, HttpStatus.OK);
    }

    /**
     * Find all File by Their Object
     * @param uuid
     * @param request
     * @return
     */
    @RequestMapping(value = "/pictures/{uuid}/{name}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> findAll(@PathVariable("uuid") String uuid,@PathVariable("name") String name, HttpServletRequest request) {
        ResponseBean responseBean = new ResponseBean();
        try {
            String token=request.getHeader(Messages.token_name);
            String doneBy=request.getHeader("doneBy");
            if(token.equalsIgnoreCase(Messages.token)){
                 responseBean.setCode(Messages.SUCCESS_CODE);
                 responseBean.setObject(fileSaverService.findByRefenceNameAndReferenceUuid(name,uuid));
            }else{
                responseBean.setCode(Messages.ERROR_CODE);
                responseBean.setDescription(Messages.incorrect_token);
            }

        }catch (Exception ex) {

        }
        return new ResponseEntity<Object>(responseBean, HttpStatus.OK);
    }

   /**
    * 
    * @param param
    * @param uuid
    * @return
    * @throws IOException
    */
	@RequestMapping(value="/download/{uuid}",method=RequestMethod.GET)
	 public ResponseEntity<Resource> download(String param, @PathVariable("uuid") String uuid) throws IOException {   
				   FileSaver fileSaver=fileSaverService.findByUuid(uuid); 
				   String filePath = fileSaver.getPath();
				   File file = new File(filePath); 
				   HttpHeaders headers = new HttpHeaders();
				    headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
				    headers.add("Pragma", "no-cache");
				    headers.add("Expires", "0");
				    headers.add("Content-Disposition", "inline; filename=" + file.getName()); 
				    Path path = Paths.get(filePath);
				    byte[] b = Files.readAllBytes((path));
				    ByteArrayInputStream bis = new ByteArrayInputStream(b);
				    InputStreamResource resource = new InputStreamResource(new FileInputStream(file));
				 
				   // InputStreamResource resource = new InputStreamResource(new FileInputStream(file));
				    return ResponseEntity.ok().headers(headers).contentLength(file.length())
				        .contentType(MediaType.parseMediaType("application/octet-stream")).body(new InputStreamResource(bis));
		
	}
	
	

}
