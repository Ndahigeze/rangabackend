package com.auca.ranga.controller;

import com.auca.ranga.domain.Comment;
import com.auca.ranga.domain.Property;
import com.auca.ranga.service.CommentService;
import com.auca.ranga.service.PropertyService;
import com.auca.ranga.utility.Messages;
import com.auca.ranga.utility.ResponseBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/comments")
public class CommentController {

    @Autowired
    private PropertyService propertyService;
    @Autowired
    private CommentService commentService;

    /**
     * Saving Comments
     * @param comment
     * @param request
     * @return
     */
    @RequestMapping(value = "/save", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> create(@RequestBody InnerComment comment, HttpServletRequest request) {
        ResponseBean responseBean = new ResponseBean();
        try {
            String token=request.getHeader(Messages.token_name);
            String doneBy=request.getHeader("doneBy");
            if(token.equalsIgnoreCase(Messages.token)){

                Optional<Property> p=Optional.ofNullable(propertyService.findByUuid(comment.property));
                if(!p.isPresent()){
                    responseBean.setCode(Messages.ERROR_CODE);
                    responseBean.setDescription("Property does not exist");
                }else{
                    Comment c=new Comment();
                    c.setClientUuid(comment.clientUuid);
                    c.setComments(comment.comment);
                    c.setProperty(p.get());
                    c.setClientEmail(comment.clientEmail);
                    c.setClientName(comment.clientName);
                    c.setDoneBy(doneBy);
                    c.setLastUpdatedBy(doneBy);
                    commentService.createOrUpdate(c);
                    responseBean.setCode(Messages.SUCCESS_CODE);
                    responseBean.setDescription("Comment submited");
                    responseBean.setObject(c);
                }
            }else{
                responseBean.setCode(Messages.ERROR_CODE);
                responseBean.setDescription(Messages.incorrect_token);
            }

        }catch (Exception ex) {

        }
        return new ResponseEntity<Object>(responseBean, HttpStatus.OK);
    }


    /**
     * Finding comment By property
     * @param uuid
     * @param request
     * @return
     */
    @RequestMapping(value = "/properties/{puuid}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> findAll(@PathVariable("puuid")String uuid, HttpServletRequest request) {
        ResponseBean responseBean = new ResponseBean();
        try {
            String token=request.getHeader(Messages.token_name);
            String doneBy=request.getHeader("doneBy");
            if(token.equalsIgnoreCase(Messages.token)){

                Optional<Property> property=Optional.ofNullable(propertyService.findByUuid(uuid));
                if(property.isPresent()){
                    responseBean.setCode(Messages.SUCCESS_CODE);
                    responseBean.setObject(commentService.findByProperty(property.get()));
                }else{
                    responseBean.setCode(Messages.ERROR_CODE);
                    responseBean.setDescription("Property not found");
                }
            }else{
                responseBean.setCode(Messages.ERROR_CODE);
                responseBean.setDescription(Messages.incorrect_token);
            }

        }catch (Exception ex) {

            ex.printStackTrace();
            responseBean.setCode(Messages.ERROR_CODE);
            responseBean.setDescription(Messages.error);
        }
        return new ResponseEntity<Object>(responseBean, HttpStatus.OK);
    }

    public static class InnerComment{
        private String property;
        private String comment;
        private String clientUuid;
        private String clientName;
        private String clientEmail;

        public String getClientName() {
            return clientName;
        }

        public void setClientName(String clientName) {
            this.clientName = clientName;
        }

        public String getClientEmail() {
            return clientEmail;
        }

        public void setClientEmail(String clientEmail) {
            this.clientEmail = clientEmail;
        }

        public String getProperty() {
            return property;
        }

        public void setProperty(String property) {
            this.property = property;
        }

        public String getComment() {
            return comment;
        }

        public void setComment(String comment) {
            this.comment = comment;
        }

        public String getClientUuid() {
            return clientUuid;
        }

        public void setClientUuid(String clientUuid) {
            this.clientUuid = clientUuid;
        }
    }
}
