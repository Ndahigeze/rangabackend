package com.auca.ranga.controller;


import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.auca.ranga.domain.ERequestStatus;
import com.auca.ranga.domain.FileSaver;
import com.auca.ranga.domain.RenewRequest;
import com.auca.ranga.service.FileSaverService;
import com.auca.ranga.service.PropertyService;
import com.auca.ranga.service.RenewRequestService;
import com.auca.ranga.service.RequestService;
import com.auca.ranga.serviceImpl.PropertyHistoryService;
import com.auca.ranga.utility.EmailHandling;
import com.auca.ranga.utility.MailObject;
import com.auca.ranga.utility.Messages;
import com.auca.ranga.utility.ResponseBean;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/renew")
public class RenewController {
    @Autowired
    private RequestService requestService;
    @Autowired
    private PropertyService propertyService;
    @Autowired
    private FileSaverService fileService;

    @Autowired
    private PropertyHistoryService pService;

    @Autowired
    private RenewRequestService renewService;



    /**
     * Find By Agent
     * 
     * @param uuid
     * @param request
     * @return
     */
    @RequestMapping(value = "/agent/{uuid}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> findByAgent(@PathVariable("uuid") String uuid, HttpServletRequest request) {
        ResponseBean responseBean = new ResponseBean();
        try {
            String token = request.getHeader(Messages.token_name);
            String doneBy = request.getHeader("doneBy");
            if (token.equalsIgnoreCase(Messages.token)) {
                responseBean.setObject(renewService.findByRequestAgentUuid(uuid));
                responseBean.setCode(Messages.SUCCESS_CODE);
            } else {
                responseBean.setCode(Messages.ERROR_CODE);
                responseBean.setDescription(Messages.incorrect_token);
            }

        } catch (Exception ex) {

            ex.printStackTrace();
            responseBean.setCode(Messages.ERROR_CODE);
            responseBean.setDescription(Messages.error);
        }
        return new ResponseEntity<Object>(responseBean, HttpStatus.OK);
    }

    /**
     * Find BY Client
     * 
     * @param uuid
     * @param request
     * @return
     */
    @RequestMapping(value = "/client/{uuid}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> findByClient(@PathVariable("uuid") String uuid, HttpServletRequest request) {
        ResponseBean responseBean = new ResponseBean();
        try {
            String token = request.getHeader(Messages.token_name);
            String doneBy = request.getHeader("doneBy");
            if (token.equalsIgnoreCase(Messages.token)) {
                responseBean.setObject(renewService.findByRequestClientUuid(uuid));
                responseBean.setCode(Messages.SUCCESS_CODE);
            } else {
                responseBean.setCode(Messages.ERROR_CODE);
                responseBean.setDescription(Messages.incorrect_token);
            }

        } catch (Exception ex) {

            ex.printStackTrace();
            responseBean.setCode(Messages.ERROR_CODE);
            responseBean.setDescription(Messages.error);
        }
        return new ResponseEntity<Object>(responseBean, HttpStatus.OK);
    }

    /**
     * Find ByUUID
     * 
     * @param uuid
     * @param request
     * @return
     */
    @RequestMapping(value = "/{uuid}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> findByUuid(@PathVariable("uuid") String uuid, HttpServletRequest request) {
        ResponseBean responseBean = new ResponseBean();
        try {
            String token = request.getHeader(Messages.token_name);
            String doneBy = request.getHeader("doneBy");
            if (token.equalsIgnoreCase(Messages.token)) {
                responseBean.setObject(renewService.findByUuid(uuid));
                responseBean.setCode(Messages.SUCCESS_CODE);
            } else {
                responseBean.setCode(Messages.ERROR_CODE);
                responseBean.setDescription(Messages.incorrect_token);
            }

        } catch (Exception ex) {

            ex.printStackTrace();
            responseBean.setCode(Messages.ERROR_CODE);
            responseBean.setDescription(Messages.error);
        }
        return new ResponseEntity<Object>(responseBean, HttpStatus.OK);
    }

    @RequestMapping(value = "/changestatus", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> changeStatus(@RequestBody StatusObject info, HttpServletRequest request) {
        ResponseBean responseBean = new ResponseBean();
        try {
            String token = request.getHeader("ranga_token");

            if (token.equalsIgnoreCase(Messages.token)) {
                RenewRequest r = renewService.findByUuid(info.requestUuid);
                if (r != null) {
                    if (info.status.equalsIgnoreCase("ACCEPT")) {
                        r.setRequestStatus(ERequestStatus.APPROVED);
                        responseBean.setDescription("REQUEST IS APPROVED SUCCESSFULLY");
                    } else if (info.status.equalsIgnoreCase("REJECT")) {
                        r.setRequestStatus(ERequestStatus.REJECTED);
                        responseBean.setDescription("REQUEST IS REJECTED SUCCESSFULLY");
                    } else if (info.status.equalsIgnoreCase("CANCEL")) {
                        r.setRequestStatus(ERequestStatus.CANCELED);
                        responseBean.setDescription("REQUEST IS CANCELLED SUCCESSFULLY");
                    } else if (info.status.equalsIgnoreCase("CLOSE")) {
                        r.setRequestStatus(ERequestStatus.CLOSED);
                        responseBean.setDescription("REQUEST IS CLOSED SUCCESSFULLY");
                    }
                    r.setLastUpdatedBy(info.username);
                    renewService.createOrUpdate(r);
                    responseBean.setCode(Messages.SUCCESS_CODE);
                    responseBean.setObject(r);
                } else {
                    responseBean.setCode(Messages.ERROR_CODE);
                    responseBean.setDescription(Messages.not_found);
                }
            } else {
                responseBean.setCode(Messages.ERROR_CODE);
                responseBean.setDescription(Messages.incorrect_token);
            }

        } catch (Exception ex) {

            ex.printStackTrace();
            responseBean.setCode(Messages.ERROR_CODE);
            responseBean.setDescription(Messages.error);
        }
        return new ResponseEntity<Object>(responseBean, HttpStatus.OK);
    }


    /**
     * Saving Request File
     * 
     * @param uuid
     * @param file
     * @param request
     * @return
     */
    @RequestMapping(value = "/attach/{uuid}", method = RequestMethod.POST)
    public ResponseEntity<Object> atachFile(@RequestParam Map<String, String> map, @PathVariable("uuid") String uuid,
            MultipartFile file, HttpServletRequest request) {
        ResponseBean responseBean = new ResponseBean();
        try {
            String token = request.getHeader(Messages.token_name);
            String username = request.getHeader("doneBy");
            if (token != null) {
                if (token.equalsIgnoreCase(Messages.token)) {
                    RenewRequest p = renewService.findByUuid(uuid);
                    FileSaver f = new FileSaver();

                    if (p == null) {
                        responseBean.setCode(Messages.ERROR_CODE);
                        responseBean.setDescription("REQUEST NOT FOUND");
                        responseBean.setObject(null);
                    } else {
                        if (uuid == null) {
                            responseBean.setCode(Messages.ERROR_CODE);
                            responseBean.setDescription("NO FILE SENT");
                            responseBean.setObject(null);
                        } else if (file == null) {
                            responseBean.setCode(Messages.ERROR_CODE);
                            responseBean.setDescription("NO FILE SENT");
                            responseBean.setObject(null);
                        } else {
                            String path = fileService.saveRequestFile(file, "", p.getUuid());
                            f.setRefenceName("FILE");
                            f.setReferenceUuid(p.getUuid());
                            f.setLastUpdatedBy(username);
                            f.setDescription(map.get("description"));
                            f.setPath(path);
                            fileService.createOrUpdate(f);
                            responseBean.setCode(Messages.SUCCESS_CODE);
                            responseBean.setDescription("FIRE IS ATACHED TO THIS REQUEST SUCCESSFULL");
                            responseBean.setObject(null);
                        }
                    }

                } else {
                    responseBean.setCode(Messages.INCORRECT_TOKEN);
                    responseBean.setDescription("Incorrect token ");
                    responseBean.setObject(null);
                }
            } else {
                responseBean.setCode(Messages.TOKEN_NOT_FOUND);
                responseBean.setDescription(" TOKEN NOT FOUND ");
                responseBean.setObject(null);
            }
        } catch (Exception ex) {
            // Todo: correct the error

            responseBean.setCode(Messages.ERROR_CODE);
            responseBean.setDescription(Messages.error);
            responseBean.setObject(null);
        }
        return new ResponseEntity<Object>(responseBean, HttpStatus.OK);
    }

    /**
     * Files Download
     */
    @RequestMapping(value = "/file/{uuid}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> findByFile(@PathVariable("uuid") String uuid, HttpServletRequest request) {
        ResponseBean responseBean = new ResponseBean();
        try {
            String token = request.getHeader(Messages.token_name);
            // String doneBy = request.getHeader("doneBy");
            if (token.equalsIgnoreCase(Messages.token)) {
                responseBean.setObject(fileService.findByRefenceNameAndReferenceUuid("FILE", uuid));
                responseBean.setCode(Messages.SUCCESS_CODE);
            } else {
                responseBean.setCode(Messages.ERROR_CODE);
                responseBean.setDescription(Messages.incorrect_token);
            }

        } catch (Exception ex) {

            ex.printStackTrace();
            responseBean.setCode(Messages.ERROR_CODE);
            responseBean.setDescription(Messages.error);
        }
        return new ResponseEntity<Object>(responseBean, HttpStatus.OK);
    }

    public void sendEmail(String agentEmail, String pno, String typ) {
        MailObject m = new MailObject();
        m.setTo(agentEmail);
        m.setSubject("Property Request");
        m.setMessage("<h2>Property Request</h2>" + "<p>A " + typ + " of N #: " + pno
                + " Has been request, please Login to PPSS Platform to handle this Request </p>");
        EmailHandling.sendmail(m);
    }

    public void sendMail(String agentEmail, String msg, String subject) {
        MailObject m = new MailObject();
        m.setTo(agentEmail);
        m.setSubject("Property Request");
        m.setMessage(msg);
        EmailHandling.sendmail(m);
    }



    public static class StatusObject {

        private String requestUuid;
        private String status;
        private String comment;
        private String username;
        private String agentUuid;

        public String getUsername() {
            return username;
        }

        public String getAgentUuid() {
            return agentUuid;
        }

        public void setAgentUuid(String agentUuid) {
            this.agentUuid = agentUuid;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getRequestUuid() {
            return requestUuid;
        }

        public void setRequestUuid(String requestUuid) {
            this.requestUuid = requestUuid;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getComment() {
            return comment;
        }

        public void setComment(String comment) {
            this.comment = comment;
        }
    }

}