package com.auca.ranga.controller;

import javax.servlet.http.HttpServletRequest;

import com.auca.ranga.dao.SubscriptionDao;
import com.auca.ranga.domain.PaymentObject;
import com.auca.ranga.domain.Property;
import com.auca.ranga.domain.SubCategory;
import com.auca.ranga.domain.Subscription;
import com.auca.ranga.service.PropertyService;
import com.auca.ranga.utility.BrainTreeConfiguration;
import com.auca.ranga.utility.Messages;
import com.auca.ranga.utility.ResponseBean;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.Date;
import java.util.Optional;

@RestController
@RequestMapping("/payments")
public class PaymentController {
    @Autowired
    private PropertyService propertyService;
    @Autowired
    private SubscriptionDao dao;
    

    /**
     * Sending Client Token
     * @param request
     * @return
     */
       @RequestMapping(value = "/client_token", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> clientToken(HttpServletRequest request) {
        ResponseBean responseBean = new ResponseBean();
        try {
            String token = request.getHeader(Messages.token_name);
            if (token.equalsIgnoreCase(Messages.token)) {
                
                responseBean.setDescription("THE CLIENT TOKEN ");
                responseBean.setCode(Messages.SUCCESS_CODE);
                responseBean.setObject(BrainTreeConfiguration.brainTreeAnvironment());
            } else {
                responseBean.setCode(Messages.ERROR_CODE);
                responseBean.setDescription(Messages.incorrect_token);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
            responseBean.setCode(Messages.ERROR_CODE);
            responseBean.setDescription(Messages.error);
        }
        return new ResponseEntity<Object>(responseBean, HttpStatus.OK);
    }

    @RequestMapping(value = "/checkout/{uuid}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> findAll(@PathVariable("uuid") String pUuid, @RequestBody PaymentObject nonce, HttpServletRequest request) {
        ResponseBean responseBean = new ResponseBean();
        try {

            String token = request.getHeader(Messages.token_name);
            if (token.equalsIgnoreCase(Messages.token)) {
                Optional<Property> p=Optional.ofNullable(propertyService.findByUuid(pUuid));
                Subscription subs=dao.findByCategory(SubCategory.valueOf(nonce.getAmount()));
                if(p.isPresent()){
                    BrainTreeConfiguration.ResponceObject r=BrainTreeConfiguration.createTransaction(nonce,subs.getAmount());
                    if(r.getCode()==Messages.SUCCESS_CODE){
                        p.get().setAllowedToPublish(true);
                             LocalDate l=LocalDate.now().plusMonths(subs.getMonths());
                             p.get().setRemoveDate(l);
                        propertyService.update(p.get());
                        responseBean.setDescription("PAYMENT SENT SUCCESSFULLY ");
                        responseBean.setCode(Messages.SUCCESS_CODE);
                        responseBean.setObject(r);
                    }else{
                        responseBean.setDescription(" PAYMENT FAILED, TRY AGAIN");
                        responseBean.setCode(Messages.ERROR_CODE);
                        responseBean.setObject(r);
                    }

                }else{
                    responseBean.setDescription(" PAYMENT FAILED, PROPERTY NOT FOUND");
                    responseBean.setCode(Messages.ERROR_CODE);

                }

            } else {
                responseBean.setCode(Messages.ERROR_CODE);
                responseBean.setDescription(Messages.incorrect_token);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
            responseBean.setCode(Messages.ERROR_CODE);
            responseBean.setDescription(Messages.error);
        }
        return new ResponseEntity<Object>(responseBean, HttpStatus.OK);
    }


    @RequestMapping(value = "/transactions", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> traction(HttpServletRequest request) {
        ResponseBean responseBean = new ResponseBean();
        try {

            String token = request.getHeader(Messages.token_name);
            if (token.equalsIgnoreCase(Messages.token)) {

                responseBean.setDescription("Received ALL TRANSACTION ");
                responseBean.setCode(Messages.SUCCESS_CODE);
                 responseBean.setObject(BrainTreeConfiguration.findAllTransactions());
            } else {
                responseBean.setCode(Messages.ERROR_CODE);
                responseBean.setDescription(Messages.incorrect_token);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
            responseBean.setCode(Messages.ERROR_CODE);
            responseBean.setDescription(Messages.error);
        }
        return new ResponseEntity<Object>(responseBean, HttpStatus.OK);
    }



    public static class BrainTreeNonce{
        private String clientNonce;
        private String type;
        private String lastFour;
        private String lastTwo;
        private String bin;
        private String amount;;
        private String cardType;
        private String clientEmail;
        private String clientPhone;
        private String  clientName;

        public String getAmount() {
            return amount;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public void setAmount(String amount) {
            this.amount=amount;
        }

        public String getClientNonce() {
            return clientNonce;
        }

        public String getBin() {
            return bin;
        }

        public void setBin(String bin) {
            this.bin = bin;
        }

        public String getLastTwo() {
            return lastTwo;
        }

        public void setLastTwo(String lastTwo) {
            this.lastTwo = lastTwo;
        }

        public String getLastFour() {
            return lastFour;
        }

        public void setLastFour(String lastFour) {
            this.lastFour = lastFour;
        }

        public String getCardType() {
            return cardType;
        }

        public void setCardType(String cardType) {
            this.cardType = cardType;
        }

        public void setClientNonce(String clientNonce) {
            this.clientNonce = clientNonce;
        }
        
        
    }

}