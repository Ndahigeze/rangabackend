package com.auca.ranga.controller;

import com.auca.ranga.domain.ERequestStatus;
import com.auca.ranga.domain.FileSaver;
import com.auca.ranga.domain.Property;
import com.auca.ranga.domain.PropertyHistory;
import com.auca.ranga.domain.RenewRequest;
import com.auca.ranga.domain.Request;
import com.auca.ranga.service.FileSaverService;
import com.auca.ranga.service.PropertyService;
import com.auca.ranga.service.RenewRequestService;
import com.auca.ranga.service.RequestService;
import com.auca.ranga.serviceImpl.PropertyHistoryService;
import com.auca.ranga.utility.EmailHandling;
import com.auca.ranga.utility.MailObject;
import com.auca.ranga.utility.Messages;
import com.auca.ranga.utility.ResponseBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;

import java.io.ByteArrayInputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("/requests")
public class RequestController {

    @Autowired
    private RequestService requestService;

    @Autowired
    private PropertyService propertyService;

    @Autowired
    private FileSaverService fileService;

    @Autowired
    private PropertyHistoryService pService;

    @Autowired
    private RenewRequestService renewService;

    @RequestMapping(value = "/save", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> create(@RequestBody InnerRequest request, HttpServletRequest servletRequestrequest) {
        ResponseBean responseBean = new ResponseBean();
        try {
            String token=servletRequestrequest.getHeader(Messages.token_name);
            String doneBy=servletRequestrequest.getHeader("doneBy");
            if(token.equalsIgnoreCase(Messages.token)){

                Optional<Property> p=Optional.ofNullable(propertyService.findByUuid(request.property));
                if(p.isPresent()){
                    request.setRequestType("PROPERTY");
                   Request request1=requestService.formRequest(request);
                   request1.setAgentUuid(request.agentUuid);
                   request1.setProperty(p.get());
                   request1.setDoneBy(doneBy);
                   request1.setLastUpdatedBy(doneBy);
                   int status=requestService.createOrUpdate(request1);
                    // sendEmail(p.get().getAgentEmail(),p.get().getPropertyNumber(),p.get().getType() );
                    if(status==Messages.SUCCESS_CODE){
                        p.get().setRequestTimes(p.get().getRequestTimes()+1);
                        propertyService.update(p.get());
                        responseBean.setCode(Messages.SUCCESS_CODE);
                        responseBean.setObject(request1);
                        responseBean.setDescription("Property Request Was Sent Successfully");
                    }else {
                        responseBean.setCode(Messages.ERROR_CODE);
                        responseBean.setDescription(Messages.error);
                    }
                }else {
                    responseBean.setCode(Messages.SUCCESS_CODE);
                    responseBean.setDescription("PROPERTY NOT FOUND");;
                }
            }else{
                responseBean.setCode(Messages.ERROR_CODE);
                responseBean.setDescription(Messages.incorrect_token);
            }
        }catch (Exception ex) {
            ex.printStackTrace();
            responseBean.setCode(Messages.ERROR_CODE);
            responseBean.setDescription(Messages.error);
        }
        return new ResponseEntity<Object>(responseBean, HttpStatus.OK);
    }

    /**
     * Find By Agent
     * @param uuid
     * @param request
     * @return
     */
    @RequestMapping(value = "/agent/{uuid}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> findByAgent(@PathVariable("uuid") String uuid, HttpServletRequest request) {
        ResponseBean responseBean = new ResponseBean();
        try {
            String token = request.getHeader(Messages.token_name);
            String doneBy = request.getHeader("doneBy");
            if (token.equalsIgnoreCase(Messages.token)) {
                responseBean.setObject(requestService.findByAgentId(uuid));
                responseBean.setCode(Messages.SUCCESS_CODE);
            } else {
                responseBean.setCode(Messages.ERROR_CODE);
                responseBean.setDescription(Messages.incorrect_token);
            }

        } catch (Exception ex) {

            ex.printStackTrace();
            responseBean.setCode(Messages.ERROR_CODE);
            responseBean.setDescription(Messages.error);
        }
        return new ResponseEntity<Object>(responseBean, HttpStatus.OK);
    }


    /**
     * Find BY Client
     * @param uuid
     * @param request
     * @return
     */
    @RequestMapping(value = "/client/{uuid}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> findByClient(@PathVariable("uuid") String uuid, HttpServletRequest request) {
        ResponseBean responseBean = new ResponseBean();
        try {
            String token = request.getHeader(Messages.token_name);
            String doneBy = request.getHeader("doneBy");
            if (token.equalsIgnoreCase(Messages.token)) {
                responseBean.setObject(requestService.findByClientUuid(uuid));
                responseBean.setCode(Messages.SUCCESS_CODE);
            } else {
                responseBean.setCode(Messages.ERROR_CODE);
                responseBean.setDescription(Messages.incorrect_token);
            }

        } catch (Exception ex) {

            ex.printStackTrace();
            responseBean.setCode(Messages.ERROR_CODE);
            responseBean.setDescription(Messages.error);
        }
        return new ResponseEntity<Object>(responseBean, HttpStatus.OK);
    }

    /**
     * Find ByUUID
     * @param uuid
     * @param request
     * @return
     */
    @RequestMapping(value = "/{uuid}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> findByUuid(@PathVariable("uuid") String uuid, HttpServletRequest request) {
        ResponseBean responseBean = new ResponseBean();
        try {
            String token = request.getHeader(Messages.token_name);
            String doneBy = request.getHeader("doneBy");
            if (token.equalsIgnoreCase(Messages.token)) {
                responseBean.setObject(requestService.findByUuid(uuid));
                responseBean.setCode(Messages.SUCCESS_CODE);
            } else {
                responseBean.setCode(Messages.ERROR_CODE);
                responseBean.setDescription(Messages.incorrect_token);
            }

        } catch (Exception ex) {

            ex.printStackTrace();
            responseBean.setCode(Messages.ERROR_CODE);
            responseBean.setDescription(Messages.error);
        }
        return new ResponseEntity<Object>(responseBean, HttpStatus.OK);
    }

    @RequestMapping(value = "/changestatus", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> changeStatus(@RequestBody StatusObject info, HttpServletRequest request) {
        ResponseBean responseBean = new ResponseBean();
        try {
            String token = request.getHeader("ranga_token");

            if (token.equalsIgnoreCase(Messages.token)) {
                 Request r=requestService.findByUuid(info.requestUuid);
                 if(r!=null){
                    if(info.status.equalsIgnoreCase("ACCEPT")){
                        r.setERequestStatus(ERequestStatus.APPROVED);
                        responseBean.setDescription("REQUEST IS APPROVED SUCCESSFULLY");
                    }else if(info.status.equalsIgnoreCase("REJECT")){
                        r.setERequestStatus(ERequestStatus.REJECTED);
                        responseBean.setDescription("REQUEST IS REJECTED SUCCESSFULLY");
                    }else if(info.status.equalsIgnoreCase("CANCEL")){
                        r.setERequestStatus(ERequestStatus.CANCELED);
                        responseBean.setDescription("REQUEST IS CANCELLED SUCCESSFULLY");
                    }else if(info.status.equalsIgnoreCase("CLOSE")){
                        r.setERequestStatus(ERequestStatus.CLOSED);
                        responseBean.setDescription("REQUEST IS CLOSED SUCCESSFULLY");
                    }
                    r.setLastUpdatedBy(info.username    );
                    requestService.createOrUpdate(r);
                    responseBean.setCode(Messages.SUCCESS_CODE);
                    responseBean.setObject(r);
                 }else{
                     responseBean.setCode(Messages.ERROR_CODE);
                     responseBean.setDescription(Messages.not_found);
                 }
            } else {
                responseBean.setCode(Messages.ERROR_CODE);
                responseBean.setDescription(Messages.incorrect_token);
            }

        } catch (Exception ex) {

            ex.printStackTrace();
            responseBean.setCode(Messages.ERROR_CODE);
            responseBean.setDescription(Messages.error);
        }
        return new ResponseEntity<Object>(responseBean, HttpStatus.OK);
    }
      
/**
 * Tarnsfer Ownership
 * @param uuid
 * @param request
 * @return
 */
    @RequestMapping(value = "/transfer/{uuid}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> transferOwnerShip(@PathVariable String uuid, HttpServletRequest request) {
        ResponseBean responseBean = new ResponseBean();
        try {
            String token = request.getHeader("ranga_token");

            if (token.equalsIgnoreCase(Messages.token)) {
                 Request r=requestService.findByUuid(uuid);
                 if(r!=null){ 
                    r.setOwnershipTransferd(true);
                    requestService.createOrUpdate(r);
                    responseBean.setCode(Messages.SUCCESS_CODE);
                    responseBean.setObject(r);
                    responseBean.setDescription("Property is Transfered, Waiting Client Confrimation");
                 }else{
                     responseBean.setCode(Messages.ERROR_CODE);
                     responseBean.setDescription(Messages.not_found);
                 }
            } else {
                responseBean.setCode(Messages.ERROR_CODE);
                responseBean.setDescription(Messages.incorrect_token);
            }

        } catch (Exception ex) {

            ex.printStackTrace();
            responseBean.setCode(Messages.ERROR_CODE);
            responseBean.setDescription(Messages.error);
        }
        return new ResponseEntity<Object>(responseBean, HttpStatus.OK);
    }

    /**
     * Confirm Tarnsfer Ownership
     * 
     * @param uuid
     * @param request
     * @return
     */
    @RequestMapping(value = "/confirmTransfer/{uuid}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> confirmTransfer(@PathVariable String uuid, HttpServletRequest request) {
        ResponseBean responseBean = new ResponseBean();
        try {
            String token = request.getHeader("ranga_token");

            if (token.equalsIgnoreCase(Messages.token)) {
                Request r = requestService.findByUuid(uuid);
                if (r != null) {
                    Property p=propertyService.findOne(r.getProperty().getId());
                    p.setAgent(r.getClientUuid());
                    p.setAvailable(false);
                    r.setTransferComment("Client Confirmed Ownership Transfer");
                    r.setERequestStatus(ERequestStatus.CLOSED);
                    requestService.createOrUpdate(r);
                    propertyService.update(p);
                    responseBean.setCode(Messages.SUCCESS_CODE);
                    responseBean.setObject(r);
                    responseBean.setDescription("Ownership Confirmed Successfully");
                } else {
                    responseBean.setCode(Messages.ERROR_CODE);
                    responseBean.setDescription(Messages.not_found);
                }
            } else {
                responseBean.setCode(Messages.ERROR_CODE);
                responseBean.setDescription(Messages.incorrect_token);
            }

        } catch (Exception ex) {

            ex.printStackTrace();
            responseBean.setCode(Messages.ERROR_CODE);
            responseBean.setDescription(Messages.error);
        }
        return new ResponseEntity<Object>(responseBean, HttpStatus.OK);
    }


   /**
    * Confirm Renew
    * @param uuid
    * @param request
    * @return
    */
    @RequestMapping(value = "/renew/{uuid}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> renew(@PathVariable String uuid,@RequestBody Renew rn, HttpServletRequest request) {
        ResponseBean responseBean = new ResponseBean();
        try {
            String token = request.getHeader("ranga_token");

            if (token.equalsIgnoreCase(Messages.token)) {
                Request r = requestService.findByUuid(uuid);
                if (r != null) {
                      RenewRequest rr=new RenewRequest();
                      rr.setComment(rn.comment);
                      rr.setRequest(r);
                      rr.setRequestStatus(ERequestStatus.PENDING); 
                      r.setERequestStatus(ERequestStatus.CLOSED);
                      r.setRenewed(true);
                      requestService.createOrUpdate(r) ;
                      renewService.createOrUpdate(rr);
                    //   String msg="<h2>Renew Request</h2><p>There is a Renew Request Please Login to <h1> RANGA</h1> to Respond the Request</p>";
                    //   sendMail(rr.getRequest().getProperty().getAgentEmail(), msg, "Renew Request");
                    responseBean.setCode(Messages.SUCCESS_CODE);
                    responseBean.setObject(r);
                    responseBean.setDescription("The Request is Sent Successfully");
                } else {
                    responseBean.setCode(Messages.ERROR_CODE);
                    responseBean.setDescription(Messages.not_found);
                }
            } else {
                responseBean.setCode(Messages.ERROR_CODE);
                responseBean.setDescription(Messages.incorrect_token);
            }

        } catch (Exception ex) {

            ex.printStackTrace();
            responseBean.setCode(Messages.ERROR_CODE);
            responseBean.setDescription(Messages.error);
        }
        return new ResponseEntity<Object>(responseBean, HttpStatus.OK);
    }

    /**
     * Saving Request File
     * @param uuid
     * @param file
     * @param request
     * @return
     */
     @RequestMapping(value = "/attach/{uuid}", method = RequestMethod.POST)
    public ResponseEntity<Object> atachFile(@RequestParam Map<String, String> map,@PathVariable("uuid") String uuid, MultipartFile file,
            HttpServletRequest request) {
        ResponseBean responseBean = new ResponseBean();
        try {
            String token = request.getHeader(Messages.token_name);
            String username = request.getHeader("doneBy");
            if (token != null) {
                if (token.equalsIgnoreCase(Messages.token)) {
                    Request p =requestService.findByUuid(uuid);
                    FileSaver f=new FileSaver();

                    if (p == null) {
                        responseBean.setCode(Messages.ERROR_CODE);
                        responseBean.setDescription("REQUEST NOT FOUND");
                        responseBean.setObject(null);
                    } else {
                        if (uuid == null) {
                            responseBean.setCode(Messages.ERROR_CODE);
                            responseBean.setDescription("NO FILE SENT");
                            responseBean.setObject(null);
                        } else if (file == null) {
                            responseBean.setCode(Messages.ERROR_CODE);
                            responseBean.setDescription("NO FILE SENT");
                            responseBean.setObject(null);
                        } else {
                            String path = fileService.saveRequestFile(file, "", p.getUuid());
                            f.setRefenceName("FILE");
                            f.setReferenceUuid(p.getUuid());
                            f.setLastUpdatedBy(username);
                            f.setDescription(map.get("description"));
                            f.setPath(path);
                            fileService.createOrUpdate(f);
                            responseBean.setCode(Messages.SUCCESS_CODE);
                            responseBean.setDescription(
                                    "FIRE IS ATACHED TO THIS REQUEST SUCCESSFULL");
                            responseBean.setObject(null);
                        }
                    }

                } else {
                    responseBean.setCode(Messages.INCORRECT_TOKEN);
                    responseBean.setDescription("Incorrect token ");
                    responseBean.setObject(null);
                }
            } else {
                responseBean.setCode(Messages.TOKEN_NOT_FOUND);
                responseBean.setDescription(" TOKEN NOT FOUND ");
                responseBean.setObject(null);
            }
        } catch (Exception ex) {
            // Todo: correct the error
           
            responseBean.setCode(Messages.ERROR_CODE);
            responseBean.setDescription(Messages.error);
            responseBean.setObject(null);
        }
        return new ResponseEntity<Object>(responseBean, HttpStatus.OK);
    }


    /**
     * Files Download
     */
    @RequestMapping(value = "/file/{uuid}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> findByFile(@PathVariable("uuid") String uuid, HttpServletRequest request) {
        ResponseBean responseBean = new ResponseBean();
        try {
            String token = request.getHeader(Messages.token_name);
            // String doneBy = request.getHeader("doneBy");
            if (token.equalsIgnoreCase(Messages.token)) {
                responseBean.setObject(fileService.findByRefenceNameAndReferenceUuid("FILE", uuid));
                responseBean.setCode(Messages.SUCCESS_CODE);
            } else {
                responseBean.setCode(Messages.ERROR_CODE);
                responseBean.setDescription(Messages.incorrect_token);
            }

        } catch (Exception ex) {

            ex.printStackTrace();
            responseBean.setCode(Messages.ERROR_CODE);
            responseBean.setDescription(Messages.error);
        }
        return new ResponseEntity<Object>(responseBean, HttpStatus.OK);
    }




    @RequestMapping(path = "/received/{user}/{status}/{date}", method = RequestMethod.GET)
    public ResponseEntity<Resource> reportReceived(HttpServletRequest request, @PathVariable("user") String uuid,@PathVariable("status") String status,@PathVariable("date") String date) {
        // receiptGenerator(SalesOrder sales)

        SimpleDateFormat d=new SimpleDateFormat("dd/MM/yyyy");

        Date dt=null;
        try{
            dt=d.parse(date);
        }catch (Exception ex){

        }
        HttpHeaders headers = new HttpHeaders();
        headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
        headers.add("Pragma", "no-cache");
        headers.add("Expires", "0");
        headers.add("Content-Disposition", "inline; filename=Cash_receipt_note.pdf");
        ByteArrayInputStream bis = new ByteArrayInputStream(requestService.request(requestService.findByAgentUuidAndERequestStatusAndDoneAt(uuid,status,dt)));

        return ResponseEntity.ok().headers(headers).contentType(MediaType.parseMediaType("application/pdf"))
                .body(new InputStreamResource(bis));

    }


    @RequestMapping(path = "/sent/{user}/{status}/{date}", method = RequestMethod.GET)
    public ResponseEntity<Resource> reportSent(HttpServletRequest request, @PathVariable("user") String uuid,@PathVariable("status")String status,@PathVariable("date") String date) {
        // receiptGenerator(SalesOrder sales)
        Date dt=null;
        try{
            SimpleDateFormat d=new SimpleDateFormat("dd/MM/yyyy");

            dt=d.parse(date);
        }catch (Exception ex){

        }

        HttpHeaders headers = new HttpHeaders();
        headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
        headers.add("Pragma", "no-cache");
        headers.add("Expires", "0");
        headers.add("Content-Disposition", "inline; filename=Cash_receipt_note.pdf");
        ByteArrayInputStream bis = new ByteArrayInputStream(requestService.request(requestService.findByClientUuidAndERequestStatusAndDoneAt(uuid,status,dt)));

        return ResponseEntity.ok().headers(headers).contentType(MediaType.parseMediaType("application/pdf"))
                .body(new InputStreamResource(bis));

    }

//



public void sendEmail(String agentEmail,String pno,String typ ){
    MailObject m=new MailObject();
    m.setTo(agentEmail);
    m.setSubject("Property Request");
    m.setMessage(
        "<h2>Property Request</h2>"+
        "<p>A "+ typ +" of N #: "+pno+" Has been request, please Login to PPSS Platform to handle this Request </p>"
        );
        EmailHandling.sendmail(m);
}

public void sendMail(String agentEmail,String msg,String subject) {
        MailObject m = new MailObject();
        m.setTo(agentEmail);
        m.setSubject("Property Request");
        m.setMessage(msg);
        EmailHandling.sendmail(m);
 }

    public static class InnerRequest{

        private String clientUuid;
        private String agentUuid;
        private String comment;
        private String cancelReason;
        private String rejectComment;
        private String requestType;
        private String property;
        private String clientEmail;

        public String getClientUuid() {
            return clientUuid;
        }

        public String getClientEmail() {
            return clientEmail;
        }

        public void setClientEmail(String clientEmail) {
            this.clientEmail = clientEmail;
        }

        public void setClientUuid(String clientUuid) {
            this.clientUuid = clientUuid;
        }

        public String getAgentUuid() {
            return agentUuid;
        }

        public void setAgentUuid(String agentUuid) {
            this.agentUuid = agentUuid;
        }

        public String getComment() {
            return comment;
        }

        public void setComment(String comment) {
            this.comment = comment;
        }

        public String getCancelReason() {
            return cancelReason;
        }

        public void setCancelReason(String cancelReason) {
            this.cancelReason = cancelReason;
        }

        public String getRejectComment() {
            return rejectComment;
        }

        public void setRejectComment(String rejectComment) {
            this.rejectComment = rejectComment;
        }

        public String getRequestType() {
            return requestType;
        }

        public void setRequestType(String requestType) {
            this.requestType = requestType;
        }

        public String getProperty() {
            return property;
        }

        public void setProperty(String property) {
            this.property = property;
        }
    }

    public static  class StatusObject{

        private String requestUuid;
        private String status;
        private String comment;
        private String username;
        private String agentUuid;

        public String getUsername() {
            return username;
        }

        public String getAgentUuid() {
            return agentUuid;
        }

        public void setAgentUuid(String agentUuid) {
            this.agentUuid = agentUuid;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getRequestUuid() {
            return requestUuid;
        }

        public void setRequestUuid(String requestUuid) {
            this.requestUuid = requestUuid;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getComment() {
            return comment;
        }

        public void setComment(String comment) {
            this.comment = comment;
        }
    }

    public static class Renew{
        private String comment;

        public String getComment() {
            return comment;
        }

        public void setComment(String comment) {
            this.comment = comment;
        }

    }
}
