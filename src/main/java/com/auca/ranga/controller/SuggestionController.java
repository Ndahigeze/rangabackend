package com.auca.ranga.controller;


import com.auca.ranga.domain.Property;
import com.auca.ranga.domain.Suggestion;
import com.auca.ranga.service.PropertyService;
import com.auca.ranga.service.SuggestionService;
import com.auca.ranga.utility.Messages;
import com.auca.ranga.utility.ResponseBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Optional;

@RestController
@RequestMapping("/suggestions")
public class SuggestionController {

    @Autowired
    private PropertyService propertyService;

    @Autowired
    private SuggestionService suggestionService;

    /**
     * Saving suggestions on property
     * @param suggestion
     * @param request
     * @return
     */
    @RequestMapping(value = "/save",method = RequestMethod.POST,consumes = MediaType.APPLICATION_JSON_VALUE,produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> create(@RequestBody InnerSuggetion suggestion, HttpServletRequest request){
        ResponseBean responseBean=new ResponseBean();
        try {
           String rangaToken=request.getHeader(Messages.token_name);
           String doneBy=request.getHeader("doneBy");
           if(rangaToken.equalsIgnoreCase(Messages.token)){
               Optional<Property> property=Optional.ofNullable(propertyService.findByUuid(suggestion.property));
               if(property.isPresent()){
                   Suggestion suggestion1=new Suggestion();
                   suggestion1.setClientUuid(suggestion.clientUuid);
                   suggestion1.setProperty(property.get());
                   suggestion1.setSuggestion(suggestion.suggection);
                        responseBean.setCode(Messages.SUCCESS_CODE);
                        responseBean.setDescription(suggestionService.createOrUpdate(suggestion1));
                        responseBean.setObject(suggestion1);
               }else{
                        responseBean.setCode(Messages.ERROR_CODE);
                        responseBean.setDescription("Property not Found");
               }
           }else{
               responseBean.setCode(Messages.ERROR_CODE);
               responseBean.setDescription(Messages.incorrect_token);
           }
        }catch (Exception ex){
            ex.printStackTrace();
            responseBean.setCode(Messages.ERROR_CODE);
            responseBean.setDescription(Messages.error);
        }
        return new ResponseEntity<>(responseBean, HttpStatus.OK);
    }


    /**
     * Find Suggestion on Property
     * @param uuid
     * @param request
     * @return
     */
    @RequestMapping(value = "/properties/{puuid}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> findAll(@PathVariable("puuid")String uuid, HttpServletRequest request) {
        ResponseBean responseBean = new ResponseBean();
        try {
            String token=request.getHeader(Messages.token_name);
            String doneBy=request.getHeader("doneBy");
            if(token.equalsIgnoreCase(Messages.token)){

                Optional<Property> property=Optional.ofNullable(propertyService.findByUuid(uuid));
                if(property.isPresent()){
                    responseBean.setCode(Messages.SUCCESS_CODE);
                    responseBean.setObject(suggestionService.findByProperty(property.get()));
                }else{
                    responseBean.setCode(Messages.ERROR_CODE);
                    responseBean.setDescription("Property not found");
                }

            }else{
                responseBean.setCode(Messages.ERROR_CODE);
                responseBean.setDescription(Messages.incorrect_token);
            }

        }catch (Exception ex) {

            ex.printStackTrace();
            responseBean.setCode(Messages.ERROR_CODE);
            responseBean.setDescription(Messages.error);
        }
        return new ResponseEntity<Object>(responseBean, HttpStatus.OK);
    }



    public static class InnerSuggetion{

        private String property;
        private String suggection;
        private String clientUuid;

        public String getProperty() {
            return property;
        }

        public void setProperty(String property) {
            this.property = property;
        }

        public String getSuggection() {
            return suggection;
        }

        public void setSuggection(String suggection) {
            this.suggection = suggection;
        }

        public String getClientUuid() {
            return clientUuid;
        }

        public void setClientUuid(String clientUuid) {
            this.clientUuid = clientUuid;
        }
    }
}
