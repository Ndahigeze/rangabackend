package com.auca.ranga.controller;

import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import com.auca.ranga.service.AdminEntityService;
import com.auca.ranga.utility.Messages;
import com.auca.ranga.utility.ResponseBean;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.auca.ranga.domain.*;
@RestController
@RequestMapping("/adminentities")
public class AdminEntityController {
    
    @Autowired
    private AdminEntityService adService;

    @RequestMapping(value = "", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> findAll( HttpServletRequest request) {
        ResponseBean responseBean = new ResponseBean();
        try {
            String token = request.getHeader(Messages.token_name);
            if (token.equalsIgnoreCase(Messages.token)) {

                responseBean.setCode(Messages.SUCCESS_CODE);
                responseBean.setObject(adService.findAll());
            } else {
                responseBean.setCode(Messages.ERROR_CODE);
                responseBean.setDescription(Messages.incorrect_token);
            }

        } catch (Exception ex) {
               ex.printStackTrace();
                 responseBean.setCode(Messages.ERROR_CODE);
                 responseBean.setDescription(Messages.error);
        }
        return new ResponseEntity<Object>(responseBean, HttpStatus.OK);
    }



    @RequestMapping(value = "/id", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> findOne(@PathVariable("id")String id,HttpServletRequest request) {
        ResponseBean responseBean = new ResponseBean();
        try {
            String token = request.getHeader(Messages.token_name);
            if (token.equalsIgnoreCase(Messages.token)) {
                 Optional<AdminEntity> op=Optional.ofNullable(adService.findOne(id));
                 if(op.isPresent()){
                    responseBean.setCode(Messages.SUCCESS_CODE);
                    responseBean.setObject(op.get());
                 }else{
                    responseBean.setCode(Messages.NOT_FOUND);
                    responseBean.setObject("LOCATION NOT FOUND");
                 }
                
            } else {
                responseBean.setCode(Messages.ERROR_CODE);
                responseBean.setDescription(Messages.incorrect_token);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
            responseBean.setCode(Messages.ERROR_CODE);
            responseBean.setDescription(Messages.error);
        }
        return new ResponseEntity<Object>(responseBean, HttpStatus.OK);
    }


}