package com.auca.ranga.controller;

import com.auca.ranga.domain.*;
import com.auca.ranga.service.AdminEntityService;
import com.auca.ranga.service.FileSaverService;
import com.auca.ranga.service.PropertyService;
import com.auca.ranga.utility.Messages;
import com.auca.ranga.utility.ResponseBean;
import org.springframework.core.io.Resource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

@RestController
@RequestMapping("/properties")
public class PropertyController {

    @Autowired
    private PropertyService propertyService;

    @Autowired
    private FileSaverService fileSaverService;

    @Autowired
    private AdminEntityService locationService;

    /**
     * Saving property
     * @param request
     * @return
     */
    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public ResponseEntity<Object> create( @RequestParam Map<String, String> map,@RequestParam(name = "file", required = false) MultipartFile file,HttpServletRequest request) {
        ResponseBean responseBean = new ResponseBean();
        try {
            String token=request.getHeader(Messages.token_name);
            String doneBy=request.getHeader("doneBy");
           if(token.equalsIgnoreCase(Messages.token)){

                Optional<Property> p=Optional.ofNullable(propertyService.findByPropertyNumber(map.get("propertyNumber")));
                Property property=new Property();
               if(p.isPresent()){
                   responseBean.setCode(Messages.ERROR_CODE);
                   responseBean.setDescription("Property of this Number exist");
               }else{
                  
                   AdminEntity location=locationService.findOne(map.get("sector"));
                       property.setPropertyNumber(map.get("propertyNumber"));
                       property.setAddress(map.get("address"));
                       property.setComment(map.get("description"));
                       property.setCurrency(ECurrency.valueOf(map.get("currency")));
                       property.setDoneBy(doneBy);
                       property.setEPriceStatus(EPriceStatus.valueOf(map.get("priceStatus")));
                       property.setEAvailableFor(EAvailableFor.valueOf(map.get("availableFor")));
                       property.setType(map.get("type"));
                       property.setAgent(map.get("agent"));
                       property.setAgentEmail(map.get("agentEmail"));
                       property.setLocation(location);
                       property.setPrice(Double.parseDouble(map.get("price")));
                       String path=fileSaverService.saveProfile(file, "oldFile",property.getPropertyNumber());
                        property.setProfile(path);
                            responseBean.setCode(Messages.SUCCESS_CODE);
                            responseBean.setDescription(propertyService.create(property));
                            responseBean.setObject(property);
               }
           }else{
               responseBean.setCode(Messages.ERROR_CODE);
               responseBean.setDescription(Messages.incorrect_token);
           }
        }catch (Exception ex) {
            ex.printStackTrace();
            responseBean.setCode(Messages.ERROR_CODE);
            responseBean.setDescription(ex.getMessage());
        }
        return new ResponseEntity<Object>(responseBean, HttpStatus.OK);
    }


    /**
     * Update property
     * @param uuid
     * @param property
     * @param request
     * @return
     */
    @RequestMapping(value = "/update/{uuid}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> update(@PathVariable("uuid") String uuid,@RequestBody InnerProperty property, HttpServletRequest request) {
        ResponseBean responseBean = new ResponseBean();
        try {
            String token=request.getHeader(Messages.token_name);
            String doneBy=request.getHeader("doneBy");
            if(token.equalsIgnoreCase(Messages.token)){

                Optional<Property> p=Optional.ofNullable(propertyService.findByUuid(uuid));
                if(p.isPresent()){
                    AdminEntity entity=locationService.findOne(property.sector);
//                    property.set(property.address);
                    p.get().setId(p.get().getId());
                    p.get().setLastUpdatedBy(doneBy);
                    p.get().setDoneBy(doneBy);
                    p.get().setLastUpdatedBy(doneBy);
                    p.get().setEAvailableFor(EAvailableFor.valueOf(property.availableFor));
                    p.get().setCurrency(ECurrency.valueOf(property.currecy));
                    p.get().setPropertyNumber(property.propertyNumber);
                    p.get().setComment(property.description);
                    p.get().setType(property.propertyCategory);
                    p.get().setAddress(property.address);
                    p.get().setLocation(entity);
                    p.get().setPrice(Double.parseDouble(property.price));
                    propertyService.update(p.get());
                    responseBean.setCode(Messages.SUCCESS_CODE);
                    responseBean.setDescription(Messages.update);
                    responseBean.setObject(property);
                }else{
                    responseBean.setCode(Messages.NOT_FOUND);
                    responseBean.setDescription(Messages.not_found);;
                }
            }else{
                responseBean.setCode(Messages.ERROR_CODE);
                responseBean.setDescription(Messages.incorrect_token);
            }

        }catch (Exception ex) {
            ex.printStackTrace();
            responseBean.setCode(Messages.ERROR_CODE);
            responseBean.setDescription(Messages.error);
        }
        return new ResponseEntity<Object>(responseBean, HttpStatus.OK);
    }


    /**
     * SAVING lONGITUDE and latitude
      * @param address
     * @param request
     * @return
     */
  @RequestMapping(value = "/update/address", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> updateAddress(@RequestBody InnerAddress address , HttpServletRequest request) {
        ResponseBean responseBean = new ResponseBean();
        try {
            String token = request.getHeader(Messages.token_name);
            String doneBy = request.getHeader("doneBy");
            if (token.equalsIgnoreCase(Messages.token)) {

                Optional<Property> p = Optional.ofNullable(propertyService.findByUuid(address.uuid));
                if (p.isPresent()) {

                      p.get().setLongitude(address.longitude);
                     p.get().setLatitude(address.latitude);
                     p.get().setLastUpdatedBy(doneBy);
                     p.get().setDoneBy(doneBy);
                    propertyService.update(p.get());
                    responseBean.setCode(Messages.SUCCESS_CODE);
                    responseBean.setDescription(Messages.update);
                    responseBean.setObject(p.get());
                } else {
                    responseBean.setCode(Messages.NOT_FOUND);
                    responseBean.setDescription(Messages.not_found);
                    ;
                }
            } else {
                responseBean.setCode(Messages.ERROR_CODE);
                responseBean.setDescription(Messages.incorrect_token);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
            responseBean.setCode(Messages.ERROR_CODE);
            responseBean.setDescription(Messages.error);
        }
        return new ResponseEntity<Object>(responseBean, HttpStatus.OK);
    }


    /**
     * Publish Property
     * @param uuid
     * @param request
     * @return
     */
    @RequestMapping(value = "/publish/{status}/{uuid}", method = RequestMethod.PUT)
    public ResponseEntity<Object> publish(@PathVariable("status") String status,@PathVariable("uuid")String uuid,HttpServletRequest request) {
        ResponseBean responseBean = new ResponseBean();
        try {
            String token=request.getHeader(Messages.token_name);
            String doneBy=request.getHeader("doneBy");
            if(token.equalsIgnoreCase(Messages.token)){
                Optional<Property> p=Optional.ofNullable(propertyService.findByUuid(uuid));
                if(p.isPresent()){
                      p.get().setAvailable(true);
                      if(status.equalsIgnoreCase("TRUE")){
                          p.get().setAvailable(true);
                          propertyService.update(p.get());
                           if(p.get().isAllowedToPublish()){
                               responseBean.setCode(Messages.SUCCESS_CODE);
                               responseBean.setDescription("PROPERTY IS PUBLISHED SUCCESSFULL");
                               responseBean.setObject(p.get());
                           }else{
                               responseBean.setCode(Messages.SUCCESS_CODE);
                               responseBean.setDescription("PAY SUBSCRIPTION FOR YOUR PROPERTY TO BE SUCCESSFULLY PUBLISHED");
                               responseBean.setObject(p.get());
                           }

                      }else{
                          p.get().setAvailable(false);
                          propertyService.update(p.get());
                          responseBean.setCode(Messages.SUCCESS_CODE);
                          responseBean.setDescription("PROPERTY IS UNPUBLISHED SUCCESSFULL");
                          responseBean.setObject(p.get());
                      }

                }else{
                    responseBean.setCode(Messages.NOT_FOUND);
                    responseBean.setDescription(Messages.not_found);;
                }
            }else{
                responseBean.setCode(Messages.ERROR_CODE);
                responseBean.setDescription(Messages.incorrect_token);
            }

        }catch (Exception ex) {
            ex.printStackTrace();
            responseBean.setCode(Messages.ERROR_CODE);
            responseBean.setDescription(Messages.error);
        }
        return new ResponseEntity<Object>(responseBean, HttpStatus.OK);
    }



    /**
     * Deleting Property
     * @param uuid
     * @param request
     * @return
     */
    @RequestMapping(value = "/delete/{uuid}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> delete(@PathVariable("uuid") String uuid, HttpServletRequest request) {
        ResponseBean responseBean = new ResponseBean();
        try {
            String token=request.getHeader(Messages.token_name);
            String doneBy=request.getHeader("doneBy");
            if(token.equalsIgnoreCase(Messages.token)){

                Optional<Property> p=Optional.ofNullable(propertyService.findByUuid(uuid));
                if(p.isPresent()){
                    Property property=new Property();
                    property=p.get();
                    property.setDoneBy(p.get().getDoneBy());
                    property.setLastUpdatedBy(doneBy);
                    property.setDoneBy(doneBy);
                    property.setLastUpdatedBy(doneBy);
                    property.setDeletedStatus(true);
                    propertyService.update(property);
                    responseBean.setCode(Messages.SUCCESS_CODE);
                    responseBean.setDescription(Messages.update);
                    responseBean.setObject(property);
                }else{
                    responseBean.setCode(Messages.NOT_FOUND);
                    responseBean.setDescription("The Property you want to delete is not found");
                }
            }else{
                responseBean.setCode(Messages.ERROR_CODE);
                responseBean.setDescription(Messages.incorrect_token);
            }

        }catch (Exception ex) {

            ex.printStackTrace();
            responseBean.setCode(Messages.ERROR_CODE);
            responseBean.setDescription(Messages.error);
        }
        return new ResponseEntity<Object>(responseBean, HttpStatus.OK);
    }




    /**
     * Getting all properties
     * @param request
     * @return
     */
    @RequestMapping(value = "", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> findAll(HttpServletRequest request) {
        ResponseBean responseBean = new ResponseBean();
        try {
            String token=request.getHeader(Messages.token_name);
            String doneBy=request.getHeader("doneBy");
            if(token.equalsIgnoreCase(Messages.token)){

                List<Property>  properties=propertyService.findAll();
                responseBean.setCode(Messages.SUCCESS_CODE);
                responseBean.setObject(properties);
            }else{
                responseBean.setCode(Messages.ERROR_CODE);
                responseBean.setDescription(Messages.incorrect_token);
            }

        }catch (Exception ex) {

            ex.printStackTrace();
            responseBean.setCode(Messages.ERROR_CODE);
            responseBean.setDescription(Messages.error);
        }
        return new ResponseEntity<Object>(responseBean, HttpStatus.OK);
    }

    /**
     * Getting all property
     * @param request
     * @return
     */
    @RequestMapping(value = "/{uuid}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> findOne(@PathVariable("uuid")String uuid,HttpServletRequest request) {
        ResponseBean responseBean = new ResponseBean();
        try {
            String token=request.getHeader(Messages.token_name);
            String doneBy=request.getHeader("doneBy");
            if(token!=null){
                if(token.equalsIgnoreCase(Messages.token)){
                    Property  property=propertyService.findByUuid(uuid);
                    responseBean.setCode(Messages.SUCCESS_CODE);
                    responseBean.setObject(property);
                }else{
                    responseBean.setCode(Messages.ERROR_CODE);
                    responseBean.setDescription("NOT ALLOWED TO ACCESS THIS SERVICE");
                }
            }else{
                responseBean.setCode(Messages.ERROR_CODE);
                responseBean.setDescription("NOT ALLOWED TO ACCESS THIS SERVICE");
            }

        }catch (Exception ex) {

            ex.printStackTrace();
            responseBean.setCode(Messages.ERROR_CODE);
            responseBean.setDescription(Messages.error);
        }
        return new ResponseEntity<Object>(responseBean, HttpStatus.OK);
    }


    /**
     * Find by Agent
     * @param uuid
     * @param request
     * @return
     */
    @RequestMapping(value = "/agent/{uuid}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> findByAngent(@PathVariable("uuid")String uuid,HttpServletRequest request) {
        ResponseBean responseBean = new ResponseBean();
        try {
            String token=request.getHeader(Messages.token_name);
            String doneBy=request.getHeader("doneBy");
            if(token.equalsIgnoreCase(Messages.token)){

                List<Property>  properties=propertyService.findByAgent(uuid);
                responseBean.setCode(Messages.SUCCESS_CODE);
                responseBean.setObject(properties);
            }else{
                responseBean.setCode(Messages.ERROR_CODE);
                responseBean.setDescription(Messages.incorrect_token);
            }

        }catch (Exception ex) {

            ex.printStackTrace();
            responseBean.setCode(Messages.ERROR_CODE);
            responseBean.setDescription(Messages.error);
        }
        return new ResponseEntity<Object>(responseBean, HttpStatus.OK);
    }



    /**
     * Find by RequestTime
     * @param request
     * @return
     */
    @RequestMapping(value = "/requested", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> findRequestTime(HttpServletRequest request) {
        ResponseBean responseBean = new ResponseBean();
        try {
            String token=request.getHeader(Messages.token_name);
            String doneBy=request.getHeader("doneBy");
            if(token.equalsIgnoreCase(Messages.token)){

                List<Property>  properties=propertyService.findByRequestTimes();
                responseBean.setCode(Messages.SUCCESS_CODE);
                responseBean.setObject(properties);
            }else{
                responseBean.setCode(Messages.ERROR_CODE);
                responseBean.setDescription(Messages.incorrect_token);
            }

        }catch (Exception ex) {

            ex.printStackTrace();
            responseBean.setCode(Messages.ERROR_CODE);
            responseBean.setDescription(Messages.error);
        }
        return new ResponseEntity<Object>(responseBean, HttpStatus.OK);
    }




    @GetMapping(value = "/profile/{uuid}")
    public ResponseEntity<InputStreamResource> getImage(@PathVariable("uuid") String uuid) throws IOException {

        String workingDir = System.getProperty("user.dir");
        Property p=propertyService.findByUuid(uuid);

        String filePath = workingDir + "/" +p.getProfile().replaceAll("\\\\", "/");
        System.out.println(filePath);
        File file = new File(filePath);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
        headers.add("Pragma", "no-cache");
        headers.add("Expires", "0");
        headers.add("Content-Disposition", "inline; filename=" + file.getName());
        Path path = Paths.get(filePath);
        byte[] b = Files.readAllBytes((path));
        ByteArrayInputStream bis = new ByteArrayInputStream(b);

        // Files.rea
        // InputStreamResource resource = new InputStreamResource(new
        // FileInputStream(file));
        return ResponseEntity.ok().headers(headers).contentLength(file.length()).contentType(MediaType.IMAGE_PNG)
                .body(new InputStreamResource(bis));
    }


    @GetMapping(value = "/images/{uuid}")
    public ResponseEntity<InputStreamResource> getImages(@PathVariable("uuid") String uuid) throws IOException {

        String workingDir = System.getProperty("user.dir");
        FileSaver p=fileSaverService.findByUuid(uuid);

        String filePath = workingDir + "/" +p.getPath().replaceAll("\\\\", "/");
        System.out.println(filePath);
        File file = new File(filePath);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
        headers.add("Pragma", "no-cache");
        headers.add("Expires", "0");
        headers.add("Content-Disposition", "inline; filename=" + file.getName());
        Path path = Paths.get(filePath);
        byte[] b = Files.readAllBytes((path));
        ByteArrayInputStream bis = new ByteArrayInputStream(b);

        // Files.rea
        // InputStreamResource resource = new InputStreamResource(new
        // FileInputStream(file));
        return ResponseEntity.ok().headers(headers).contentLength(file.length()).contentType(MediaType.IMAGE_PNG)
                .body(new InputStreamResource(bis));
    }


    /**
     * Update Profile
     * @param uuid
     * @param file
     * @param request
     * @return
     */
    @RequestMapping(value = "/updateProfile/{uuid}", method = RequestMethod.POST)
    public ResponseEntity<Object> updateProfile(@PathVariable("uuid") String uuid, MultipartFile file,
                                                HttpServletRequest request) {
        ResponseBean responseBean = new ResponseBean();
        try {
            String token = request.getHeader(Messages.token_name);
            String username = request.getHeader("doneBy");
            if (token != null) {
                if (token.equalsIgnoreCase(Messages.token)) {
                    Property p = propertyService.findByUuid(uuid);
                    if (p == null) {
                        responseBean.setCode(Messages.ERROR_CODE);
                        responseBean.setDescription("PROPERTY NOT FOUND");
                        responseBean.setObject(null);
                    } else {
                        if (uuid == null) {
                            responseBean.setCode(Messages.ERROR_CODE);
                            responseBean.setDescription("NO FILE SENT");
                            responseBean.setObject(null);
                        } else if (file == null) {
                            responseBean.setCode(Messages.ERROR_CODE);
                            responseBean.setDescription("NO FILE SENT");
                            responseBean.setObject(null);
                        } else {
                            String path=fileSaverService.saveProfile(file, p.getProfile(),p.getPropertyNumber());
                            p.setProfile(path);
                            p.setLastUpdatedBy(username);
                            propertyService.update(p);
                            responseBean.setCode(Messages.SUCCESS_CODE);
                            responseBean.setDescription("PICTURE IS SAVED NOW, IT WILL BE THE FIRST TO APPEAR ON HOME PAGE");
                            responseBean.setObject(null);
                        }
                    }

                } else {
                    responseBean.setCode(Messages.INCORRECT_TOKEN);
                    responseBean.setDescription("Incorrect token ");
                    responseBean.setObject(null);
                }
            } else {
                responseBean.setCode(Messages.TOKEN_NOT_FOUND);
                responseBean.setDescription(" TOKEN NOT FOUND ");
                responseBean.setObject(null);
            }
        } catch (Exception ex) {
            // Todo: correct the error
            System.out.println("RegistrantController (saveProfile) " + ex.getMessage());
            responseBean.setCode(Messages.ERROR_CODE);
            responseBean.setDescription(Messages.error);
            responseBean.setObject(null);
        }
        return new ResponseEntity<Object>(responseBean, HttpStatus.OK);
    }


    /**
     * Saving Pictures
     * @param uuid
     * @param files
     * @param request
     * @return
     */
    @RequestMapping(value = "/pictures/{uuid}", method = RequestMethod.POST)
    public ResponseEntity<Object> updateLoan(@PathVariable("uuid")String uuid,@RequestParam("file") MultipartFile[] files,  HttpServletRequest request) {
        ResponseBean rs = new ResponseBean();
        try {

            String userToken = request.getHeader(Messages.token_name);
            if (userToken == null) {
                rs.setCode(Messages.ERROR_CODE);
                rs.setDescription(Messages.incorrect_token);
                rs.setObject(null);
            } else if (!userToken.equalsIgnoreCase(Messages.token)) {
                rs.setCode(Messages.INCORRECT_TOKEN);
                rs.setDescription(Messages.incorrect_token);
                rs.setObject(null);
            } else {
                Property p = propertyService.findByUuid(uuid);
                if (p == null) {
                    rs.setCode(Messages.ERROR_CODE);
                    rs.setDescription("PROPERTY NOT FOUND");
                    rs.setObject(null);
                } else {

                    List<MultipartFile> fil = Arrays.asList(files);
                    if (!fil.isEmpty()) {
                        int i=0;
                            for (MultipartFile v: fil) {
                                i++;
                                String path=fileSaverService.saveProfile(v, "",p.getPropertyNumber()+""+i);

                                FileSaver f=new FileSaver();
                                f.setPath(path);
                                f.setRefenceName("PICTURE");
                                f.setReferenceUuid(p.getUuid());
                                fileSaverService.createOrUpdate(f);
                            }
                        }else{
                                rs.setCode(Messages.ERROR_CODE);
                                rs.setDescription("NO FILE SENT");
                     }
                    }
                    rs.setCode(Messages.SUCCESS_CODE);
                    rs.setDescription("success");

                }

        } catch (Exception e) {
            e.printStackTrace();
            rs.setCode(Messages.ERROR_CODE);
            rs.setDescription(Messages.error);
            rs.setObject(null);
        }

        return new ResponseEntity<>(rs, HttpStatus.OK);
    }


        @RequestMapping(value = "/allowed", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> findAllAllowed(HttpServletRequest request) {
        ResponseBean responseBean = new ResponseBean();
        try {
            String token=request.getHeader(Messages.token_name);
            String doneBy=request.getHeader("doneBy");
            if(token.equalsIgnoreCase(Messages.token)){

                List<Property>  properties=propertyService.findByDeletedStatusAndAllowedToPublishAndAvailable(false,true,true);
                responseBean.setCode(Messages.SUCCESS_CODE);
                responseBean.setObject(properties);
            }else{
                responseBean.setCode(Messages.ERROR_CODE);
                responseBean.setDescription(Messages.incorrect_token);
            }

        }catch (Exception ex) {

            ex.printStackTrace();
            responseBean.setCode(Messages.ERROR_CODE);
            responseBean.setDescription(Messages.error);
        }
        return new ResponseEntity<Object>(responseBean, HttpStatus.OK);
    }

   

    

    private static class InnerAddress{
        private double longitude;
        private double latitude;
        private String uuid;

        public double getLongitude() {
            return longitude;
        }

        public void setLongitude(double longitude) {
            this.longitude = longitude;
        }

        public double getLotitude() {
            return latitude;
        }

        public void setLatitude(double latitude) {
            this.latitude = latitude;
        }

        public String getUuid() {
            return uuid;
        }

        public void setUuid(String uuid) {
            this.uuid = uuid;
        }
    }



    public static class InnerProperty{

        private String availableFor;
        private String description;
        private String propertyCategory;
        private String propertyNumber;
        private String price;
        private String sector;
        private String address;
        private String currecy;
        private String agentEmail;

        public String getAvailableFor() {
            return availableFor;
        }

        public String getAgentEmail() {
            return agentEmail;
        }

        public void setAgentEmail(String agentEmail) {
            this.agentEmail = agentEmail;
        }

        public void setAvailableFor(String availableFor) {
            this.availableFor = availableFor;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getPropertyNumber() {
            return propertyNumber;
        }

        public String getCurrecy() {
            return currecy;
        }

        public void setCurrecy(String currecy) {
            this.currecy = currecy;
        }


        /**
         * @return String return the description
         */
        public String getDescription() {
            return description;
        }

        /**
         * @return String return the propertyCategory
         */
        public String getPropertyCategory() {
            return propertyCategory;
        }

        /**
         * @param propertyCategory the propertyCategory to set
         */
        public void setPropertyCategory(String propertyCategory) {
            this.propertyCategory = propertyCategory;
        }

        /**
         * @param propertyNumber the propertyNumber to set
         */
        public void setPropertyNumber(String propertyNumber) {
            this.propertyNumber = propertyNumber;
        }

        /**
         * @return String return the price
         */
        public String getPrice() {
            return price;
        }

        /**
         * @param price the price to set
         */
        public void setPrice(String price) {
            this.price = price;
        }

        /**
         * @return String return the sector
         */
        public String getSector() {
            return sector;
        }

        /**
         * @param sector the sector to set
         */
        public void setSector(String sector) {
            this.sector = sector;
        }

        /**
         * @return String return the address
         */
        public String getAddress() {
            return address;
        }

        /**
         * @param address the address to set
         */
        public void setAddress(String address) {
            this.address = address;
        }

    }


}
