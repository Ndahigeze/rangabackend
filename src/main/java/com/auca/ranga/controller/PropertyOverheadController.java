package com.auca.ranga.controller;


import com.auca.ranga.domain.ECurrency;
import com.auca.ranga.domain.Property;
import com.auca.ranga.domain.PropertyOverhead;
import com.auca.ranga.service.PropertyOverheadService;
import com.auca.ranga.service.PropertyService;
import com.auca.ranga.utility.Messages;
import com.auca.ranga.utility.ResponseBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Optional;

@RestController
@RequestMapping("/overheads")
public class PropertyOverheadController {
    @Autowired
    private PropertyOverheadService overheadService;
    @Autowired
    private PropertyService propertyService;

    @RequestMapping(value = "/save",method = RequestMethod.POST,consumes = MediaType.APPLICATION_JSON_VALUE,produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> create(@RequestBody InnerOverhead overhead, HttpServletRequest request){
        ResponseBean responseBean=new ResponseBean();
        try {
            String rangaToken=request.getHeader("token");
            String doneBy=request.getHeader("doneBy");
            if(rangaToken.equalsIgnoreCase(Messages.token)){
                Optional<Property> property=Optional.ofNullable(propertyService.findByUuid(overhead.property));
                if(property.isPresent()){
                    PropertyOverhead overhead1=new PropertyOverhead();
                    overhead1.setAmount(overhead.amount);
                    overhead1.setDescription(overhead.description);
                    overhead1.setName(overhead.name);
                    overhead1.setProperty(property.get());
                    overhead1.setCurrency(ECurrency.valueOf(overhead.currency));
                    responseBean.setCode(Messages.SUCCESS_CODE);
                    responseBean.setDescription(overheadService.createOrUpdate(overhead1));
                    responseBean.setObject(overhead1);
                }else{
                    responseBean.setCode(Messages.ERROR_CODE);
                    responseBean.setDescription("Property not Found");
                }
            }else{
                responseBean.setCode(Messages.ERROR_CODE);
                responseBean.setDescription(Messages.incorrect_token);
            }
        }catch (Exception ex){
            ex.printStackTrace();
            responseBean.setCode(Messages.ERROR_CODE);
            responseBean.setDescription(Messages.error);
        }
        return new ResponseEntity<>(responseBean, HttpStatus.OK);
    }


    /**
     * Updatang Overhead
     * @param uuid
     * @param overhead
     * @param request
     * @return
     */
    @RequestMapping(value = "/update/{uuid}",method = RequestMethod.PUT,consumes = MediaType.APPLICATION_JSON_VALUE,produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> update(@PathVariable("uuid")String uuid,@RequestBody InnerOverhead overhead, HttpServletRequest request){
        ResponseBean responseBean=new ResponseBean();
        try {
            String rangaToken=request.getHeader("token");
            String doneBy=request.getHeader("doneBy");
            if(rangaToken.equalsIgnoreCase(Messages.token)){
                Optional<PropertyOverhead> o=Optional.ofNullable(overheadService.findByUuid(uuid));
                if(o.isPresent()){
                    o.get().setCurrency(ECurrency.valueOf(overhead.currency));
                    o.get().setDescription(overhead.description);
                    o.get().setAmount(overhead.amount);
                    o.get().setName(overhead.name);
                    overheadService.createOrUpdate(o.get());
                    responseBean.setCode(Messages.SUCCESS_CODE);
                    responseBean.setDescription(Messages.update);
                    responseBean.setObject(o.get());
                }else{
                    responseBean.setCode(Messages.ERROR_CODE);
                    responseBean.setDescription("OVERHEAD NOT FOUND");
                }
            }else{
                responseBean.setCode(Messages.ERROR_CODE);
                responseBean.setDescription(Messages.incorrect_token);
            }
        }catch (Exception ex){
            ex.printStackTrace();
            responseBean.setCode(Messages.ERROR_CODE);
            responseBean.setDescription(Messages.error);
        }
        return new ResponseEntity<>(responseBean, HttpStatus.OK);
    }


    /**
     * Deleting OH
     * @param uuid
     * @param request
     * @return
     */
    @RequestMapping(value = "/delete/{uuid}",method = RequestMethod.DELETE,consumes = MediaType.APPLICATION_JSON_VALUE,produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> delete(@PathVariable("uuid")String uuid, HttpServletRequest request){
        ResponseBean responseBean=new ResponseBean();
        try {
            String rangaToken=request.getHeader("token");
            String doneBy=request.getHeader("doneBy");
            if(rangaToken.equalsIgnoreCase(Messages.token)){
                Optional<PropertyOverhead> o=Optional.ofNullable(overheadService.findByUuid(uuid));
                if(o.isPresent()){
                    o.get().setDeletedStatus(true);
                    overheadService.createOrUpdate(o.get());
                    responseBean.setCode(Messages.SUCCESS_CODE);
                    responseBean.setDescription(Messages.delete);
                    responseBean.setObject(o.get());
                }else{
                    responseBean.setCode(Messages.ERROR_CODE);
                    responseBean.setDescription("OVERHEAD NOT FOUND");
                }
            }else{
                responseBean.setCode(Messages.ERROR_CODE);
                responseBean.setDescription(Messages.incorrect_token);
            }
        }catch (Exception ex){
            ex.printStackTrace();
            responseBean.setCode(Messages.ERROR_CODE);
            responseBean.setDescription(Messages.error);
        }
        return new ResponseEntity<>(responseBean, HttpStatus.OK);
    }




    /**
     * Find Overhead by Property
     * @param uuid
     * @param request
     * @return
     */
    @RequestMapping(value = "/properties/{puuid}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> findAll(@PathVariable("puuid")String uuid, HttpServletRequest request) {
        ResponseBean responseBean = new ResponseBean();
        try {
            String token=request.getHeader(Messages.token_name);
            String doneBy=request.getHeader("doneBy");
            if(token.equalsIgnoreCase(Messages.token)){

                Optional<Property> property=Optional.ofNullable(propertyService.findByUuid(uuid));
                if(property.isPresent()){
                    responseBean.setCode(Messages.SUCCESS_CODE);
                    responseBean.setObject(overheadService.findByProperty(property.get()));
                }else{
                    responseBean.setCode(Messages.ERROR_CODE);
                    responseBean.setDescription("Property not found");
                }

            }else{
                responseBean.setCode(Messages.ERROR_CODE);
                responseBean.setDescription(Messages.incorrect_token);
            }

        }catch (Exception ex) {

            ex.printStackTrace();
            responseBean.setCode(Messages.ERROR_CODE);
            responseBean.setDescription(Messages.error);
        }
        return new ResponseEntity<Object>(responseBean, HttpStatus.OK);
    }


    public static class InnerOverhead{
        private String property;
        private String name;
        private String description;
        private double amount;
        private String currency;

        public String getCurrency() {
            return currency;
        }

        public void setCurrency(String currency) {
            this.currency = currency;
        }

        public String getProperty() {
            return property;
        }

        public void setProperty(String property) {
            this.property = property;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public double getAmount() {
            return amount;
        }

        public void setAmount(double amount) {
            this.amount = amount;
        }
    }
}
