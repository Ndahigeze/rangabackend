package com.auca.ranga.controller;

import com.auca.ranga.domain.Property;
import com.auca.ranga.domain.PropertyDetails;
import com.auca.ranga.service.PropertyDescriptionService;
import com.auca.ranga.service.PropertyService;
import com.auca.ranga.utility.Messages;
import com.auca.ranga.utility.ResponseBean;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/propertyDescriptions")
public class PropertyDescriptionController {

    @Autowired
    private PropertyDescriptionService propertyDescriptionService;

    @Autowired
    private PropertyService ps;

    @RequestMapping(value = "/save", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> create(@RequestBody InnerDetails d, HttpServletRequest request) {
        ResponseBean responseBean = new ResponseBean();
        try {
            String token=request.getHeader("token");
            String doneBy=request.getHeader("doneBy");
            if(token.equalsIgnoreCase(Messages.token)){
                Optional<Property> p=Optional.ofNullable(ps.findByUuid(d.property));
                    if(p.isPresent()){
                        PropertyDetails pd=new PropertyDetails();

                             pd.setDoneBy(doneBy);
                             pd.setLastUpdatedBy(doneBy);
                             pd.setProperty(p.get());
                             pd.setDescription(d.description);
                             pd.setName(d.name);
                             System.out.println(pd.getName());
                                responseBean.setCode(Messages.SUCCESS_CODE);
                                  responseBean.setDescription(propertyDescriptionService.createOrUpdate(pd));
                                  responseBean.setObject(pd);
                    }else{
                         responseBean.setCode(Messages.ERROR_CODE);
                         responseBean.setDescription("PROPERTY NOT FOUND");
                         responseBean.setObject(null);
                    }
            }else{
                responseBean.setCode(Messages.ERROR_CODE);
                responseBean.setDescription(Messages.incorrect_token);
            }

        }catch (Exception ex) {
                ex.printStackTrace();
               responseBean.setCode(Messages.ERROR_CODE);
              responseBean.setDescription(Messages.error);
        }
        return new ResponseEntity<Object>(responseBean, HttpStatus.OK);
    }

    /**
     * updating the Description
     * @param uuid
     * @param description
     * @param request
     * @return
     */
    @RequestMapping(value = "/update/{uuid}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> update(@PathVariable("uuid") String uuid, @RequestBody InnerDetails description, HttpServletRequest request) {
        ResponseBean responseBean = new ResponseBean();
        try {
            String token=request.getHeader("token");
            String doneBy=request.getHeader("doneBy");
            if(token.equalsIgnoreCase(Messages.token)){

                Optional<PropertyDetails> p=Optional.ofNullable(propertyDescriptionService.findByUuid(uuid));
                if(p.isPresent()){

                    p.get().setLastUpdatedBy(doneBy);
                    p.get().setDescription(description.description);
                    p.get().setName(description.name);
                    propertyDescriptionService.createOrUpdate(p.get());
                       responseBean.setCode(Messages.SUCCESS_CODE);
                       responseBean.setDescription(Messages.update);
                       responseBean.setObject(description);
                }else{
                    responseBean.setCode(Messages.NOT_FOUND);
                    responseBean.setDescription(Messages.not_found);;
                }
            }else{
                responseBean.setCode(Messages.ERROR_CODE);
                responseBean.setDescription(Messages.incorrect_token);
            }

        }catch (Exception ex) {

            ex.printStackTrace();
            responseBean.setCode(Messages.ERROR_CODE);
            responseBean.setDescription(Messages.error);
        }
        return new ResponseEntity<Object>(responseBean, HttpStatus.OK);
    }


    /**
     * Deleted description
     * @param uuid
     * @param request
     * @return
     */
    @RequestMapping(value = "/delete/{uuid}", method = RequestMethod.DELETE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> delete(@PathVariable("uuid") String uuid, HttpServletRequest request) {
        ResponseBean responseBean = new ResponseBean();
        try {
            String token=request.getHeader("token");
            String doneBy=request.getHeader("doneBy");
            if(token.equalsIgnoreCase(Messages.token)){

                Optional<PropertyDetails> p=Optional.ofNullable( propertyDescriptionService.findByUuid(uuid));
                if(p.isPresent()){
                    PropertyDetails description=new PropertyDetails();
                    description=p.get();
                    description.setDoneBy(p.get().getDoneBy());
                    description.setLastUpdatedBy(doneBy);
                    description.setDoneBy(doneBy);
                    description.setLastUpdatedBy(doneBy);
                    description.setDeletedStatus(true);
                    propertyDescriptionService.createOrUpdate(description);
                    responseBean.setCode(Messages.SUCCESS_CODE);
                    responseBean.setDescription(Messages.delete);
                    responseBean.setObject(description);
                }else{
                    responseBean.setCode(Messages.NOT_FOUND);
                    responseBean.setDescription("The Property you want to delete is not found");
                }
            }else{
                responseBean.setCode(Messages.ERROR_CODE);
                responseBean.setDescription(Messages.incorrect_token);
            }

        }catch (Exception ex) {

            ex.printStackTrace();
            responseBean.setCode(Messages.ERROR_CODE);
            responseBean.setDescription(Messages.error);
        }
        return new ResponseEntity<Object>(responseBean, HttpStatus.OK);
    }


    /**
     * Find by Property
     * @param uuid
     * @param request
     * @return
     */
    @RequestMapping(value = "property/{uuid}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> findAll(@PathVariable("uuid")String uuid, HttpServletRequest request) {
        ResponseBean responseBean = new ResponseBean();
        try {
            String token=request.getHeader(Messages.token_name);
            String doneBy=request.getHeader("doneBy");
            if(token.equalsIgnoreCase(Messages.token)){

                List<PropertyDetails> descriptionList=propertyDescriptionService.findByProperty(uuid);
                responseBean.setCode(Messages.SUCCESS_CODE);
                responseBean.setObject(descriptionList);
            }else{
                responseBean.setCode(Messages.ERROR_CODE);
                responseBean.setDescription(Messages.incorrect_token);
            }

        }catch (Exception ex) {

            ex.printStackTrace();
            responseBean.setCode(Messages.ERROR_CODE);
            responseBean.setDescription(Messages.error);
        }
        return new ResponseEntity<Object>(responseBean, HttpStatus.OK);
    }



    public static class InnerDetails{
         private String property;
         private String name;
         private String description;
         
        /**
         * @return String return the property
         */
        public String getProperty() {
            return property;
        }

        /**
         * @param property the property to set
         */
        public void setProperty(String property) {
            this.property = property;
        }

        /**
         * @return String return the name
         */
        public String getName() {
            return name;
        }

        /**
         * @param name the name to set
         */
        public void setName(String name) {
            this.name = name;
        }

        /**
         * @return String return the description
         */
        public String getDescription() {
            return description;
        }

    }


}
