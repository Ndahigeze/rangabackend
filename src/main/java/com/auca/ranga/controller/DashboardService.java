package com.auca.ranga.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.auca.ranga.domain.*;

import com.auca.ranga.service.PropertyService;
import com.auca.ranga.service.RenewRequestService;
import com.auca.ranga.service.RequestService;
import com.auca.ranga.utility.BrainTreeConfiguration;
import com.auca.ranga.utility.Messages;
import com.auca.ranga.utility.ResponseBean;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/dashboard")
public class DashboardService {

    @Autowired
   private PropertyService ps;

   @Autowired
   private RequestService requestService;

   @Autowired
   private RenewRequestService renewService;
   
    @RequestMapping(value = "/{uuid}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> showInfo(@PathVariable("uuid") String uuid,
            HttpServletRequest request) {
        ResponseBean responseBean = new ResponseBean();
        try {
            String token = request.getHeader(Messages.token_name);
            // String doneBy = request.getHeader("doneBy");
            if (token.equalsIgnoreCase(Messages.token)) {
                  Analytic a=new Analytic();
                    List<Request> rr=requestService.findByAgentId(uuid);
                    List<Request> sr=requestService.findByClientUuid(uuid);
                    List<Property> p=ps.findByAgent(uuid);
                    List<RenewRequest> rn=renewService.findByRequestAgentUuid(uuid);
                    List<RenewRequest> rsn=renewService.findByRequestClientUuid(uuid);
                     a.property=p;
                   for(Request r: rr){
                     if(r.getERequestStatus().equals(ERequestStatus.PENDING)){
                        a.pendingReceivedRequest+=1;
                     }else if(r.getERequestStatus().equals(ERequestStatus.APPROVED)){
                        a.acceptedReceivedRequest+=1;
                     }else if(r.getERequestStatus().equals(ERequestStatus.REJECTED)){
                        a.rejectedReceivedRequest+=1;
                     }else if(r.getERequestStatus().equals(ERequestStatus.CLOSED)){
                        a.closedReceivedRequest+=1;
                     }else if(r.getERequestStatus().equals(ERequestStatus.CANCELED)){
                         a.cancelledReceivedRequest+=1;
                     }
                    } 
                        
                    for (Request r : sr) {
                        if (r.getERequestStatus().equals(ERequestStatus.PENDING)) {
                            a.pendingSentRequest += 1;
                        } else if (r.getERequestStatus().equals(ERequestStatus.APPROVED)) {
                            a.acceptedSentRequest += 1;
                        } else if (r.getERequestStatus().equals(ERequestStatus.REJECTED)) {
                            a.rejectedSentRequest += 1;
                        } else if (r.getERequestStatus().equals(ERequestStatus.CLOSED)) {
                            a.closedSentRequest += 1;
                        }else if(r.getERequestStatus().equals(ERequestStatus.CANCELED)){
                            a.cancelledSentRequest+=1;
                        }
                    }

                    for(RenewRequest r: rn){
                       if (r.getRequestStatus().equals(ERequestStatus.PENDING)) {
                            a.renewReceivedPending += 1;
                        } else if (r.getRequestStatus().equals(ERequestStatus.APPROVED)) {
                            a.renewReceivedAccepted += 1;
                        } else if (r.getRequestStatus().equals(ERequestStatus.REJECTED)) {
                            a.renewReceivedRejected += 1;
                        } else if (r.getRequestStatus().equals(ERequestStatus.CLOSED)) {
                            a.renewReceivedClosed += 1;
                        }else if(r.getRequestStatus().equals(ERequestStatus.CANCELED)){
                            a.renewReceivedCanceled +=1;
                        }  
                    }

                    for (RenewRequest r : rsn) {
                        if (r.getRequestStatus().equals(ERequestStatus.PENDING)) {
                            a.renewSentPending += 1;
                        } else if (r.getRequestStatus().equals(ERequestStatus.APPROVED)) {
                            a.renewSentAccepted += 1;
                        } else if (r.getRequestStatus().equals(ERequestStatus.REJECTED)) {
                            a.renewSentRejected += 1;
                        } else if (r.getRequestStatus().equals(ERequestStatus.CLOSED)) {
                            a.renewSentClosed += 1;
                        }else if(r.getRequestStatus().equals(ERequestStatus.CANCELED)){
                            a.renewSentCanceled+=1;
                        }
                    }
                        a.renewedRequest=rn.size()+rsn.size();
                        a.properties=p.size();
                        a.receivedRequest=rr.size();
                        a.sentRequest=sr.size();
                    responseBean.setCode(Messages.SUCCESS_CODE);
                    responseBean.setDescription(Messages.update);
                    responseBean.setObject(a);
             
            } else {
                responseBean.setCode(Messages.ERROR_CODE);
                responseBean.setDescription(Messages.incorrect_token);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
            responseBean.setCode(Messages.ERROR_CODE);
            responseBean.setDescription(Messages.error);
        }
        return new ResponseEntity<Object>(responseBean, HttpStatus.OK);
    }


    @RequestMapping(value = "/owner/{uuid}",produces = MediaType.APPLICATION_JSON_VALUE,method = RequestMethod.GET)
    public ResponseEntity<Object> ShowPublished(@PathVariable("uuid")String uuid, HttpServletRequest request){
        ResponseBean rs=new ResponseBean();
        try{
            String token=request.getHeader("ranga_token");
            if(token!=null){
                if(token.equalsIgnoreCase(Messages.token)){
                    Map<String,Object> map=new HashMap<String,Object>();
                    map.put("publishedProperties",ps.findbyUserAndAllowedToPublish(uuid));
                    rs.setCode(Messages.SUCCESS_CODE);
                    rs.setDescription("PUBLISHED RECORDS FOUND");
                    rs.setObject(map);

                }else{
                    rs.setObject(null);
                    rs.setCode(Messages.ERROR_CODE);
                    rs.setDescription("INCORRECT TOKEN");
                }
            }else{
                rs.setObject(null);
                rs.setCode(Messages.ERROR_CODE);
                rs.setDescription("INCORRECT TOKEN");
            }
        }catch (Exception ex){
            ex.printStackTrace();
            rs.setObject(null);
            rs.setCode(Messages.ERROR_CODE);
            rs.setDescription(Messages.error);

        }
        return new ResponseEntity<Object>(rs,HttpStatus.OK);
    }



    /**
     * Service to Return Data Needed For Admin Dashboard
     * @param request
     * @return
     */
       @RequestMapping(value = "/admin",produces = MediaType.APPLICATION_JSON_VALUE,method = RequestMethod.GET)
    public ResponseEntity<Object> showInfoAdmin(HttpServletRequest request){
        ResponseBean rs=new ResponseBean();
        try{
            String token=request.getHeader("ranga_token");
            if(token!=null){
                if(token.equalsIgnoreCase(Messages.token)){
                    Map<String,Object> map=new HashMap<String,Object>();
                    map.put("requests",requestService.findAll());
                    map.put("renewedRequests",renewService.findAll());
                    map.put("publishedProperties",ps.findByDeletedStatusAndAllowedToPublishAndAvailable(false,true,true));
                    map.put("allproperties",ps.findAll());
                        rs.setCode(Messages.SUCCESS_CODE);
                        rs.setDescription("RECORDS FOUND");
                        rs.setObject(map);

                }else{
                    rs.setObject(null);
                    rs.setCode(Messages.ERROR_CODE);
                    rs.setDescription("INCORRECT TOKEN");
                }
            }else{
                rs.setObject(null);
                rs.setCode(Messages.ERROR_CODE);
                rs.setDescription("INCORRECT TOKEN");
            }
        }catch (Exception ex){
            ex.printStackTrace();
            rs.setObject(null);
            rs.setCode(Messages.ERROR_CODE);
            rs.setDescription(Messages.error);

        }
        return new ResponseEntity<Object>(rs,HttpStatus.OK);
    }


    /**
     * view Transaction
     * @param request
     * @return
     */
    @RequestMapping(value = "/transactions", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> traction(HttpServletRequest request) {
        ResponseBean responseBean = new ResponseBean();
        try {

            String token = request.getHeader(Messages.token_name);
            if (token.equalsIgnoreCase(Messages.token)) {

                responseBean.setDescription("Received ALL TRANSACTION ");
                responseBean.setCode(Messages.SUCCESS_CODE);
                responseBean.setObject(BrainTreeConfiguration.findAllTransactions());
            } else {
                responseBean.setCode(Messages.ERROR_CODE);
                responseBean.setDescription(Messages.incorrect_token);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
            responseBean.setCode(Messages.ERROR_CODE);
            responseBean.setDescription(Messages.error);
        }
        return new ResponseEntity<Object>(responseBean, HttpStatus.OK);
    }
   
   public static class Analytic{
        private double properties;
        private List<Property> property;
        private double sentRequest;
        private double receivedRequest;
        private double renewedRequest;

        private double acceptedSentRequest;
        private double rejectedSentRequest;
        private double cancelledSentRequest;
        private double closedSentRequest;
        private double pendingSentRequest;

        private double acceptedReceivedRequest;
        private double rejectedReceivedRequest;
        private double cancelledReceivedRequest;
        private double closedReceivedRequest;
        private double pendingReceivedRequest;

        private double renewSentPending;
        private double renewSentAccepted;
        private double renewSentRejected;
        private double renewSentClosed;
        private double renewSentCanceled;

        private double renewReceivedPending;
        private double renewReceivedAccepted;
        private double renewReceivedRejected;
        private double renewReceivedClosed;
        private double renewReceivedCanceled;

        public double getPendingReceivedRequest() {
            return pendingReceivedRequest;
        }

        public List<Property> getProperty() {
            return property;
        }

        public void setProperty(List<Property> property) {
            this.property = property;
        }

        public double getRenewReceivedCanceled() {
            return renewReceivedCanceled;
        }

        public void setRenewReceivedCanceled(double renewReceivedCanceled) {
            this.renewReceivedCanceled = renewReceivedCanceled;
        }

        public double getRenewSentCanceled() {
            return renewSentCanceled;
        }

        public void setRenewSentCanceled(double renewSentCanceled) {
            this.renewSentCanceled = renewSentCanceled;
        }

        public double getProperties() {
            return properties;
        }

        public void setProperties(double properties) {
            this.properties = properties;
        }

        public double getSentRequest() {
            return sentRequest;
        }

        public void setSentRequest(double sentRequest) {
            this.sentRequest = sentRequest;
        }

        public double getReceivedRequest() {
            return receivedRequest;
        }

        public void setReceivedRequest(double receivedRequest) {
            this.receivedRequest = receivedRequest;
        }

        public double getRenewedRequest() {
            return renewedRequest;
        }

        public void setRenewedRequest(double renewedRequest) {
            this.renewedRequest = renewedRequest;
        }

        public double getAcceptedSentRequest() {
            return acceptedSentRequest;
        }

        public void setAcceptedSentRequest(double acceptedSentRequest) {
            this.acceptedSentRequest = acceptedSentRequest;
        }

        public double getRejectedSentRequest() {
            return rejectedSentRequest;
        }

        public void setRejectedSentRequest(double rejectedSentRequest) {
            this.rejectedSentRequest = rejectedSentRequest;
        }

        public double getCancelledSentRequest() {
            return cancelledSentRequest;
        }

        public void setCancelledSentRequest(double cancelledSentRequest) {
            this.cancelledSentRequest = cancelledSentRequest;
        }

        public double getClosedSentRequest() {
            return closedSentRequest;
        }

        public void setClosedSentRequest(double closedSentRequest) {
            this.closedSentRequest = closedSentRequest;
        }

        public double getPendingSentRequest() {
            return pendingSentRequest;
        }

        public void setPendingSentRequest(double pendingSentRequest) {
            this.pendingSentRequest = pendingSentRequest;
        }

        public double getAcceptedReceivedRequest() {
            return acceptedReceivedRequest;
        }

        public void setAcceptedReceivedRequest(double acceptedReceivedRequest) {
            this.acceptedReceivedRequest = acceptedReceivedRequest;
        }

        public double getRejectedReceivedRequest() {
            return rejectedReceivedRequest;
        }

        public void setRejectedReceivedRequest(double rejectedReceivedRequest) {
            this.rejectedReceivedRequest = rejectedReceivedRequest;
        }

        public double getCancelledReceivedRequest() {
            return cancelledReceivedRequest;
        }

        public void setCancelledReceivedRequest(double cancelledReceivedRequest) {
            this.cancelledReceivedRequest = cancelledReceivedRequest;
        }

        public double getClosedReceivedRequest() {
            return closedReceivedRequest;
        }

        public void setClosedReceivedRequest(double closedReceivedRequest) {
            this.closedReceivedRequest = closedReceivedRequest;
        }

        public void setPendingReceivedRequest(double pendingReceivedRequest) {
            this.pendingReceivedRequest = pendingReceivedRequest;
        }

        /**
         * @return double return the renewSentPending
         */
        public double getRenewSentPending() {
            return renewSentPending;
        }

        /**
         * @param renewSentPending the renewSentPending to set
         */
        public void setRenewSentPending(double renewSentPending) {
            this.renewSentPending = renewSentPending;
        }

        /**
         * @return double return the renewSentAccepted
         */
        public double getRenewSentAccepted() {
            return renewSentAccepted;
        }

        /**
         * @param renewSentAccepted the renewSentAccepted to set
         */
        public void setRenewSentAccepted(double renewSentAccepted) {
            this.renewSentAccepted = renewSentAccepted;
        }

        /**
         * @return double return the renewSentRejected
         */
        public double getRenewSentRejected() {
            return renewSentRejected;
        }

        /**
         * @param renewSentRejected the renewSentRejected to set
         */
        public void setRenewSentRejected(double renewSentRejected) {
            this.renewSentRejected = renewSentRejected;
        }

        /**
         * @return double return the renewSentClosed
         */
        public double getRenewSentClosed() {
            return renewSentClosed;
        }

        /**
         * @param renewSentClosed the renewSentClosed to set
         */
        public void setRenewSentClosed(double renewSentClosed) {
            this.renewSentClosed = renewSentClosed;
        }

        /**
         * @return double return the renewReceivedPending
         */
        public double getRenewReceivedPending() {
            return renewReceivedPending;
        }

        /**
         * @param renewReceivedPending the renewReceivedPending to set
         */
        public void setRenewReceivedPending(double renewReceivedPending) {
            this.renewReceivedPending = renewReceivedPending;
        }

        /**
         * @return double return the renewReceivedAccepted
         */
        public double getRenewReceivedAccepted() {
            return renewReceivedAccepted;
        }

        /**
         * @param renewReceivedAccepted the renewReceivedAccepted to set
         */
        public void setRenewReceivedAccepted(double renewReceivedAccepted) {
            this.renewReceivedAccepted = renewReceivedAccepted;
        }

        /**
         * @return double return the renewReceivedRejected
         */
        public double getRenewReceivedRejected() {
            return renewReceivedRejected;
        }

        /**
         * @param renewReceivedRejected the renewReceivedRejected to set
         */
        public void setRenewReceivedRejected(double renewReceivedRejected) {
            this.renewReceivedRejected = renewReceivedRejected;
        }

        /**
         * @return double return the renewReceivedClosed
         */
        public double getRenewReceivedClosed() {
            return renewReceivedClosed;
        }

        /**
         * @param renewReceivedClosed the renewReceivedClosed to set
         */
        public void setRenewReceivedClosed(double renewReceivedClosed) {
            this.renewReceivedClosed = renewReceivedClosed;
        }

   }

    
}