package com.auca.ranga.domain;

public enum EAvailableFor {

    RENT,
    SALE
}
