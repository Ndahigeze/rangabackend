package com.auca.ranga.domain;

public enum ERequestStatus {
    OPEN,
    APPROVED,
    REJECTED,
    CANCELED,
    PENDING,
    CLOSED,
    AGENT_CONFIRMED_TRANSFER,
    CLIENT_CONFIRMED_TRANSFER
}
