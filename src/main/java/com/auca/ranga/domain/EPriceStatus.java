package com.auca.ranga.domain;

public enum EPriceStatus {

    NEGOTIABLE,
    FIXED
}
