package com.auca.ranga.domain;


import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Date;

@Entity
@EntityListeners(AuditingEntityListener.class)  ////  doneAt and lastUpdatedAt fields should automatically get populated whenever we create or update an entity.
public class Property extends Common implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String owner;
   
    private String type;

    @Column(unique = true)
    private String propertyNumber;

    private double longitude;

    private double latitude;

    private String address;

    private int requestTimes=0;
    /**
     * Date stop publish the property
     */
    private LocalDate removeDate;

    private String propertyModel;
    /**
     * The agent who published proeprty
     */
    private String agent;

    /**
     * profile picture for the property
     */
    private String profile;

    private double price;

    // Comment or description on the property
    private String comment;

    // If proeprty is available to rent or sale
    private boolean available;

    @Enumerated(EnumType.STRING)
    private EAvailableFor EAvailableFor;

    @Enumerated(EnumType.STRING)
    private EPriceStatus EPriceStatus;

    // for properties Allowed to be shown on publish page
    private boolean allowedToPublish;

    @Enumerated(EnumType.STRING)
    private ECurrency currency;

    @ManyToOne
    private AdminEntity location;

    private String agentEmail;

    public LocalDate getRemoveDate() {
        return removeDate;
    }

    public void setRemoveDate(LocalDate removeDate) {
        this.removeDate = removeDate;
    }

    public String getPropertyModel() {
        return propertyModel;
    }

    public String getAgentEmail() {
        return agentEmail;
    }

    public void setAgentEmail(String agentEmail) {
        this.agentEmail = agentEmail;
    }

    public void setPropertyModel(String propertyModel) {
        this.propertyModel = propertyModel;
    }

    public int getRequestTimes() {
        return requestTimes;
    }

    public void setRequestTimes(int requestTimes) {
        this.requestTimes = requestTimes;
    }

    public ECurrency getCurrency() {
        return currency;
    }

    public void setCurrency(ECurrency currency) {
        this.currency = currency;
    }

    public AdminEntity getLocation() {
        return location;
    }

    public void setLocation(AdminEntity location) {
        this.location = location;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getAgent() {
        return agent;
    }

    public void setAgent(String agent) {
        this.agent = agent;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPropertyNumber() {
        return propertyNumber;
    }

    public void setPropertyNumber(String propertyNumber) {
        this.propertyNumber = propertyNumber;
    }


    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }


    public String getProfile() {
        return profile;
    }

    public void setProfile(String profile) {
        this.profile = profile;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    public EAvailableFor getEAvailableFor() {
        return EAvailableFor;
    }

    public void setEAvailableFor(EAvailableFor EAvailableFor) {
        this.EAvailableFor = EAvailableFor;
    }

    public EPriceStatus getEPriceStatus() {
        return EPriceStatus;
    }

    public void setEPriceStatus(EPriceStatus EPriceStatus) {
        this.EPriceStatus = EPriceStatus;
    }

    public boolean isAllowedToPublish() {
        return allowedToPublish;
    }

    public void setAllowedToPublish(boolean allowedToPublish) {
        this.allowedToPublish = allowedToPublish;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
