package com.auca.ranga.domain;

public enum ERequestType {

    PROPERTY,
    GENERAL
}
