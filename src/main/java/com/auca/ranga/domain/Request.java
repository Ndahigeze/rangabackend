package com.auca.ranga.domain;


import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

@Entity
@EntityListeners(AuditingEntityListener.class)  ////  doneAt and lastUpdatedAt fields should automatically get populated whenever we create or update an entity.
public class Request extends Common implements Serializable {


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @ManyToOne
    private Property property;

    private String clientUuid;
    private String agentUuid;

    @Column(length = 1000)
    private String requestComment;

    @Column(length = 1000)
    private String rejectComment;

    @Column(length = 1000)
    private String cancelReason;

    @Enumerated(EnumType.STRING)
    private ERequestStatus eRequestStatus;

    @Enumerated(EnumType.STRING)
    private ERequestType ERequestType;


    private boolean ownershipTransferd;

    private String clientEmail;

    private String transferComment;

    private boolean renewed;

    public Long getId() {
        return id;
    }

    public boolean isRenewed() {
        return renewed;
    }

    public void setRenewed(boolean renewed) {
        this.renewed = renewed;
    }

    public String getTransferComment() {
        return transferComment;
    }

    public void setTransferComment(String transferComment) {
        this.transferComment = transferComment;
    }

    public boolean isOwnershipTransferd() {
        return ownershipTransferd;
    }

    public void setOwnershipTransferd(boolean ownershipTransferd) {
        this.ownershipTransferd = ownershipTransferd;
    }

    public String getClientEmail() {
        return clientEmail;
    }

    public void setClientEmail(String clientEmail) {
        this.clientEmail = clientEmail;
    }

    public String getAgentUuid() {
        return agentUuid;
    }

    public void setAgentUuid(String agentUuid) {
        this.agentUuid = agentUuid;
    }

    public String getClientUuid() {
        return clientUuid;
    }

    public void setClientUuid(String clientUuid) {
        this.clientUuid = clientUuid;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public Property getProperty() {
        return property;
    }

    public void setProperty(Property property) {
        this.property = property;
    }

    
    public String getRequestComment() {
        return requestComment;
    }

    public void setRequestComment(String requestComment) {
        this.requestComment = requestComment;
    }

    public String getRejectComment() {
        return rejectComment;
    }

    public void setRejectComment(String rejectComment) {
        this.rejectComment = rejectComment;
    }

    public String getCancelReason() {
        return cancelReason;
    }

    public void setCancelReason(String cancelReason) {
        this.cancelReason = cancelReason;
    }

    public ERequestStatus getERequestStatus() {
        return eRequestStatus;
    }

    public void setERequestStatus(ERequestStatus eRequestStatus) {
        this.eRequestStatus = eRequestStatus;
    }

    public ERequestType getERequestType() {
        return ERequestType;
    }

    public void setERequestType(ERequestType ERequestType) {
        this.ERequestType = ERequestType;
    }
}
