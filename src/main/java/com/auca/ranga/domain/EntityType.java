package com.auca.ranga.domain;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;



import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Entity
  @EntityListeners(AuditingEntityListener.class) // doneAt and lastUpdatedAt fields should automatically get populated  whenever we create or update an entity.
@Table(name = "entitytypes")
public class EntityType {
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long typeNo;
    private String name;
    private boolean composite;

    /**
     * @return the typeNo
     */
    public long getTypeNo() {
        return typeNo;
    }

    /**
     * @return the composite
     */
    public boolean isComposite() {
        return composite;
    }

    /**
     * @param composite the composite to set
     */
    public void setComposite(boolean composite) {
        this.composite = composite;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @param typeNo the typeNo to set
     */
    public void setTypeNo(long typeNo) {
        this.typeNo = typeNo;
    }


}