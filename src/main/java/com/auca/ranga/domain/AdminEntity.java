package com.auca.ranga.domain;



import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Entity
  @EntityListeners(AuditingEntityListener.class) // doneAt and lastUpdatedAt fields should automatically get populated  whenever we create or update an entity.
@Table(name = "adminentities")
public class AdminEntity {
      
    @Id
    private String entityID;
    
   
    private String entityName;

    @ManyToOne
    @JoinColumn(name = "parent_id")
    private AdminEntity parentID;

    @ManyToOne
    @JoinColumn(name = "type_no")
    private EntityType type;

    /**
     * @return the entityID
     */
    public String getEntityID() {
        return entityID;
    }

    /**
     * @return the type
     */
    public EntityType getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(EntityType type) {
        this.type = type;
    }

    /**
     * @return the parentID
     */
    public AdminEntity getParentID() {
        return parentID;
    }

    /**
     * @param parentID the parentID to set
     */
    public void setParentID(AdminEntity parentID) {
        this.parentID = parentID;
    }

	

    /**
     * @return the entityName
     */
    public String getEntityName() {
        return entityName;
    }

    /**
     * @param entityName the entityName to set
     */
    public void setEntityName(String entityName) {
        this.entityName = entityName;
    }

    /**
     * @param entityID the entityID to set
     */
    public void setEntityID(String entityID) {
        this.entityID = entityID;
    }
}