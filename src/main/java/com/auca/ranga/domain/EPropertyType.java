package com.auca.ranga.domain;

public enum EPropertyType {
	
	HOUSE,
	AUTO,
	APARTMENT,
	LAND,
	OTHER

}
