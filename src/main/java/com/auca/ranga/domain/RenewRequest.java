package com.auca.ranga.domain;


import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@EntityListeners(AuditingEntityListener.class)
public class RenewRequest extends Common implements Serializable {


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @ManyToOne
    private Request request;

    @Column(length = 1000)
    private String comment;

    @Enumerated(EnumType.STRING) 
    private ERequestStatus requestStatus;
    @Column(length = 1000)
    private String rejectReason;

    private String cancelReason;

    public Request getRequest() {
        return request;
    }

    public String getCancelReason() {
        return cancelReason;
    }

    public void setCancelReason(String cancelReason) {
        this.cancelReason = cancelReason;
    }

    public void setRequest(Request request) {
        this.request = request;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public ERequestStatus getRequestStatus() {
        return requestStatus;
    }

    public void setRequestStatus(ERequestStatus requestStatus) {
        this.requestStatus = requestStatus;
    }

    public String getRejectReason() {
        return rejectReason;
    }

    public void setRejectReason(String rejectReason) {
        this.rejectReason = rejectReason;
    }
}
