package com.auca.ranga.domain;

public enum EAggrementStatus {
	
	OPEN,
	CLOSED,
	TERMINATED,
	AGENT_TEMINATED,
	CLIENT_TERMINATED,
	AGENT_CONFIRMED,
	CLIENT_CONFIRMED,
	CANCELLED
}
