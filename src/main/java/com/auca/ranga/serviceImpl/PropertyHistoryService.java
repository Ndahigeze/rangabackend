package com.auca.ranga.serviceImpl;

import com.auca.ranga.dao.PropertyHistoryDao;
import com.auca.ranga.domain.Property;
import com.auca.ranga.domain.PropertyHistory;
import com.auca.ranga.utility.Messages;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Transactional
@Service
public class PropertyHistoryService {

    @Autowired
    private PropertyHistoryDao dao;
    public int createOrUpdate(PropertyHistory ph) {
        try {
            dao.save(ph);
            return Messages.SUCCESS_CODE;
        }catch (Exception ex){
            ex.printStackTrace();
            return Messages.ERROR_CODE;
        }
    }

    public List<PropertyHistory> findByProperty(Property property) {
        List<PropertyHistory> requests=new ArrayList<>();
        try {
            requests=dao.findByProperty(property);
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return requests;
    }
}
