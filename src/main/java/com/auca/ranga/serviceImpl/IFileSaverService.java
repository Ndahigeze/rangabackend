package com.auca.ranga.serviceImpl;

import com.auca.ranga.dao.FileSaverDao;
import com.auca.ranga.domain.FileSaver;
import com.auca.ranga.service.FileSaverService;
import com.auca.ranga.utility.Messages;

import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;


@Service
@Transactional
public class IFileSaverService implements FileSaverService {

    @Autowired
    private FileSaverDao fileSaverDao;

    @Override
    public int createOrUpdate(FileSaver atachment) {

        try{
           fileSaverDao.save(atachment);
           return Messages.SUCCESS_CODE;
        }catch (Exception ex){
            ex.printStackTrace();
           return Messages.ERROR_CODE;
        }
    }

    @Override
    public FileSaver findOne(long id) {
        FileSaver f=null;
        try{
            f=fileSaverDao.findOne(id);
        }catch (Exception ex){
           ex.printStackTrace();
        }
        return f;
    }

    @Override
    public FileSaver findByUuid(String uuid) {
        FileSaver f=null;
        try{
          f=fileSaverDao.findByUuidAndDeletedStatus(uuid,false);
        }catch (Exception ex){
          ex.printStackTrace();
        }
        return f;
    }

    @Override
    public List<FileSaver> findByRefenceNameAndReferenceUuid(String name, String uuid) {
        return  fileSaverDao.findByRefenceNameAndReferenceUuid(name,uuid);
    }



    /**
     * Function to Save user Profile Picture
     * 
     * @param file
     * @param oldFile
     * @param names
     * @return
     */
    @Override
    public String saveProfile(MultipartFile file, String oldFile, String names) {
        String filepath = "";
//        names.replaceAll("\\\\", "");
        try {
            File user = new File("property");
            deleteOldProfile(oldFile);
            if (!user.exists()) {
                user.mkdir();
                byte[] bytes = file.getBytes();
                Path path = Paths.get(user.getPath() + "/" + names + file.getOriginalFilename());
                Files.write(path, bytes);
                filepath = path.toString();
            } else {
                byte[] bytes = file.getBytes();
                Path path = Paths.get(user.getPath() + "/" + names + file.getOriginalFilename());
                Files.write(path, bytes);
                filepath = path.toString();
            }

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }

        return filepath;
    }

    public void deleteOldProfile(String oldFile) {
        File profile = new File(oldFile);
        try {
            if (profile.exists()) {
                FileUtils.cleanDirectory(profile);
                profile.delete();
            }
        } catch (Exception ex) {
            ex.getMessage();
        }

    }

    @Override
    public String saveRequestFile(MultipartFile file, String oldFile, String names) {
        String filepath = "";
        // names.replaceAll("\\\\", "");
        try {
            File user = new File("request");
            deleteOldProfile(oldFile);
            if (!user.exists()) {
                user.mkdir();
                byte[] bytes = file.getBytes();
                Path path = Paths.get(user.getPath() + "/" + names + file.getOriginalFilename());
                Files.write(path, bytes);
                filepath = path.toString();
            } else {
                byte[] bytes = file.getBytes();
                Path path = Paths.get(user.getPath() + "/" + names + file.getOriginalFilename());
                Files.write(path, bytes);
                filepath = path.toString();
            }

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }

        return filepath;
    }

   

}
