package com.auca.ranga.serviceImpl;

import com.auca.ranga.dao.SuggestionDao;
import com.auca.ranga.domain.Property;
import com.auca.ranga.domain.Suggestion;
import com.auca.ranga.service.SuggestionService;
import com.auca.ranga.utility.Messages;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class ISuggestionService implements SuggestionService {
    @Autowired
    private SuggestionDao suggestionDao;

    @Override
    public String createOrUpdate(Suggestion suggestion) {
        try{
            suggestionDao.save(suggestion);
            return Messages.save;
        }catch (Exception ex){
            ex.printStackTrace();
            return Messages.error;
        }
    }

    @Override
    public String delete(Suggestion suggestion) {
        try{
            suggestion.setDeletedStatus(true);
            suggestionDao.save(suggestion);
            return Messages.save;
        }catch (Exception ex){
            ex.printStackTrace();
            return Messages.error;
        }
    }

    @Override
    public List<Suggestion> findByProperty(Property property) {
        List<Suggestion> suggestions=new ArrayList<>();
        try{
            suggestions=suggestionDao.findByPropertyAndDeletedStatus(property,false);
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return  suggestions;
    }
}
