package com.auca.ranga.serviceImpl;


import com.auca.ranga.dao.PropertyDescriptionDao;
import com.auca.ranga.domain.PropertyDetails;
import com.auca.ranga.service.PropertyDescriptionService;
import com.auca.ranga.utility.Messages;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class IPropertyDescriptionService implements PropertyDescriptionService {

    @Autowired
    private PropertyDescriptionDao descriptionDao;
    @Override
    public String createOrUpdate(PropertyDetails description) {
        try{
            descriptionDao.save(description);
        return Messages.save;
        }catch (Exception ex){
            ex.printStackTrace();
            return Messages.error;
        }
    }

    @Override
    public PropertyDetails findOne(long id) {
       PropertyDetails description=null;
        try{
            description=descriptionDao.findByIdAndDeletedStatus(id,false);
        }catch (Exception ex){
           ex.printStackTrace();
        }
        return description;
    }

    @Override
    public PropertyDetails findByUuid(String uuid) {
        PropertyDetails description=null;
        try{
            description=descriptionDao.findByUuidAndDeletedStatus(uuid,false);
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return description;
    }

    @Override
    public List<PropertyDetails> findByProperty(String uuid) {
        List<PropertyDetails> descriptions=new ArrayList<>();
        try{
            descriptions=descriptionDao.findByPropertyUuidAndDeletedStatus(uuid,false);
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return descriptions;
    }

}
