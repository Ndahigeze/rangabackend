package com.auca.ranga.serviceImpl;

import com.auca.ranga.dao.PropertyDao;
import com.auca.ranga.domain.Property;
import com.auca.ranga.service.PropertyService;
import com.auca.ranga.utility.Messages;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class IPropertyService implements PropertyService {

    @Autowired
   private PropertyDao propertyDao;

    @Override
    public String create(Property pr) {
        try{
          propertyDao.save(pr);
          return Messages.save;
        }catch (Exception ex){
          ex.printStackTrace();
          return Messages.error;
        }
    }

    @Override
    public String update(Property pr) {
        try{
           propertyDao.save(pr);
           return Messages.update;
        }catch (Exception ex){
            ex.printStackTrace();
            return Messages.error;
        }
    }

    @Override
    public String delete(Property pr) {
        try{
            propertyDao.save(pr);
            return Messages.update;
        }catch (Exception ex){
            ex.printStackTrace();
            return Messages.error;
        }
    }

    @Override
    public Property findOne(long id) {
        Property pr=null;
        try{
            pr=propertyDao.findByIdAndDeletedStatus(id,false);

        }catch (Exception ex){
            ex.printStackTrace();
        }
        return pr;
    }

    @Override
    public Property findByUuid(String uuid) {
        Property pr=null;
        try{
            pr=propertyDao.findByUuidAndDeletedStatus(uuid,false);

        }catch (Exception ex){
            ex.printStackTrace();
        }
        return pr;
    }

    @Override
    public Property findByPropertyNumber(String pn) {
        Property pr=null;
        try{
            pr=propertyDao.findByPropertyNumberAndDeletedStatus(pn,false);

        }catch (Exception ex){
            ex.printStackTrace();
        }
        return pr;
    }

    @Override
    public List<Property> findByAddress(String address) {
        List<Property> pr=new ArrayList<>();
        try{
            pr=propertyDao.findByDeletedStatusAndAddressContaining(false,address);
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return pr;
    }

    @Override
    public List<Property> findByAgent(String publisherUuid) {
        List<Property> pr=new ArrayList<>();
        try{
            pr=propertyDao.findByAgentAndDeletedStatus(publisherUuid,false);
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return pr;
    }

    @Override
    public List<Property> findByOwner(String ownerName) {
        List<Property> pr=new ArrayList<>();
        try{
            pr=propertyDao.findByOwnerAndDeletedStatus(ownerName,false);
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return pr;
    }

    @Override
    public List<Property> findAll() {
        List<Property> pr=new ArrayList<>();
        try{
            pr=propertyDao.findByDeletedStatus(false);
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return pr;
    }

    @Override
    public List<Property> findByDeletedStatusAndAllowedToPublishAndAvailable(boolean ds, boolean ab, boolean a) {
        List<Property> pr=new ArrayList<>();
        try{
            pr=propertyDao.findByDeletedStatusAndAllowedToPublishAndAvailable(false,true,true);
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return pr;
    }

    @Override
    public List<Property> findbyUserAndAllowedToPublish( String uuid) {
        List<Property> pr=new ArrayList<>();
        try{
            pr=propertyDao.findByAgentAndDeletedStatusAndAllowedToPublishAndAvailable(uuid,false,true,true);
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return pr;
    }

    @Override
    public List<Property> findByRequestTimes() {

        List<Property> pr=new ArrayList<>();
        try{
            pr=propertyDao.findByDeletedStatusAndAllowedToPublishAndAvailableOrderByRequestTimesDesc(false,true,true);
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return pr;
    }
//
//    @Override
//    public byte[] properties(List<Property> p) {
//        return new byte[0];
//    }
//
//    @Override
//    public byte[] propertyDetails(Property p) {
//        return new byte[0];
//    }
}
