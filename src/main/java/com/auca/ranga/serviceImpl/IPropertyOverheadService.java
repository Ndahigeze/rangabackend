package com.auca.ranga.serviceImpl;


import com.auca.ranga.dao.PropertyOverheadDao;
import com.auca.ranga.domain.Property;
import com.auca.ranga.domain.PropertyOverhead;
import com.auca.ranga.service.PropertyOverheadService;
import com.auca.ranga.utility.Messages;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class IPropertyOverheadService implements PropertyOverheadService {

    @Autowired
    private PropertyOverheadDao overheadDao;

    @Override
    public String createOrUpdate(PropertyOverhead propertyOverhead) {
        try{
            overheadDao.save(propertyOverhead);
            return Messages.save;
        }catch (Exception ex){
            return Messages.error;
        }
    }


    @Override
    public String delete(PropertyOverhead propertyOverhead) {
        try{
            overheadDao.save(propertyOverhead);
            return Messages.delete;
        }catch (Exception ex){
            return Messages.error;
        }
    }

    @Override
    public PropertyOverhead findOne(long id) {
         PropertyOverhead overhead=null;
         try {
             overhead=overheadDao.findOne(id);
         }catch (Exception ex){
               ex.printStackTrace();

         }
         return  overhead;
    }

    @Override
    public PropertyOverhead findByUuid(String uuid) {
        PropertyOverhead overhead=null;
        try {
            overhead=overheadDao.findByUuidAndDeletedStatus(uuid,false);
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return  overhead;
    }

    @Override
    public List<PropertyOverhead> findByProperty(Property property) {
        List<PropertyOverhead> overheads=new ArrayList<>();
        try {
            overheads=overheadDao.findByPropertyAndDeletedStatus(property,false);
        }catch (Exception ex){
            ex.printStackTrace();

        }
        return  overheads;
    }


}
