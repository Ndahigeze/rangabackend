package com.auca.ranga.serviceImpl;


import com.auca.ranga.domain.Message;
import com.auca.ranga.service.MessageService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class IMessageService implements MessageService {
    @Override
    public String create(Message msg) {
        return null;
    }

    @Override
    public String update(Message msg) {
        return null;
    }

    @Override
    public String delete(Message msg) {
        return null;
    }

    @Override
    public String findOne(long id) {
        return null;
    }

    @Override
    public String findByUuid(String uuid) {
        return null;
    }
}
