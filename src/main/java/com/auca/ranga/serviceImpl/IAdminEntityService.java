package com.auca.ranga.serviceImpl;

import java.util.List;

import com.auca.ranga.dao.AdminEntityDao;
import com.auca.ranga.domain.AdminEntity;
import com.auca.ranga.service.AdminEntityService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class IAdminEntityService implements AdminEntityService {
    
    @Autowired
     private AdminEntityDao dao;

    @Override
    public List<AdminEntity> findAll() {
        return dao.findAll();
    }

    @Override
    public AdminEntity findOne(String id) {
        return dao.findOne(id);
    }
    
}