package com.auca.ranga.serviceImpl;

import java.util.ArrayList;
import java.util.List;

import com.auca.ranga.dao.RenewRequestDao;
import com.auca.ranga.domain.RenewRequest;
import com.auca.ranga.domain.Request;
import com.auca.ranga.service.RenewRequestService;
import com.auca.ranga.utility.Messages;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class IRenewRequestService implements RenewRequestService {

    @Autowired
    private RenewRequestDao dao;

    @Override
    public int createOrUpdate(RenewRequest request) {
        try {
            dao.save(request);
            return Messages.SUCCESS_CODE;
        } catch (Exception ex) {
            ex.printStackTrace();
            return Messages.ERROR_CODE;
        }
    }

    @Override
    public String delete(RenewRequest renewRequest) {
        return null;
    }

    @Override
    public RenewRequest findOne(long id) {
        RenewRequest r = null;
        try {
            r = dao.findOne(id);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return r;
    }

    @Override
    public RenewRequest findByUuid(String uuid) {
        RenewRequest r = null;
        try {
            r = dao.findByUuid(uuid);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return r;
    }

    @Override
    public List<RenewRequest> findByRequestAgentUuid(String uid) {
        List<RenewRequest> r=new ArrayList<>();
        try {
            r=dao.findByRequestAgentUuid(uid);
        } catch (Exception e) {
            //TODO: handle exception
              e.printStackTrace();
        }
        return r;
    }

    @Override
    public List<RenewRequest> findByRequestClientUuid(String uid) {
        List<RenewRequest> r = new ArrayList<>();
        try {
            r = dao.findByRequestClientUuid(uid);
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }
        return r;
    }

    @Override
    public List<RenewRequest> findAll() {
        List<RenewRequest> r = new ArrayList<>();
        try {
            r = dao.findAll();
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }
        return r;
    }

    @Override
    public byte[] renewRequestReports(List<RenewRequest> r) {
        return new byte[0];
    }

    @Override
    public byte[] renewRequestDetailReport(RenewRequest p) {
        return new byte[0];
    }


}
