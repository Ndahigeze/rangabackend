package com.auca.ranga.serviceImpl;

import com.auca.ranga.controller.RequestController;
import com.auca.ranga.dao.RequestDao;
import com.auca.ranga.domain.ERequestStatus;
import com.auca.ranga.domain.ERequestType;
import com.auca.ranga.domain.Property;
import com.auca.ranga.domain.Request;
import com.auca.ranga.service.RequestService;
import com.auca.ranga.utility.Messages;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
@Transactional
public class IRequestService implements RequestService {

    @Autowired
    private RequestDao requestDao;

    @Override
    public int createOrUpdate(Request request) {
      try {
        requestDao.save(request);
        return Messages.SUCCESS_CODE;
      }catch (Exception ex){
        ex.printStackTrace();
        return Messages.ERROR_CODE;
      }
    }

    @Override
    public Request findOne(long id) {
        Request r=null;
        try {
            r=requestDao.findOne(id);
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return r;
    }

    @Override
    public Request findByUuid(String uuid) {
        Request r=null;
        try {
             r=requestDao.findByUuidAndDeletedStatus(uuid,false);
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return r;
    }

    @Override
    public List<Request> findAll() {
        List<Request> requests=new ArrayList<>();
        try {
            requests=requestDao.findAll();
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return requests;
    }

    @Override
    public List<Request> findByPropertyAndClientId(Property property,String id) {
        List<Request> requests=new ArrayList<>();
        try {
            requests=requestDao.findByPropertyAndClientUuidAndDeletedStatus(property,id,false);
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return requests;
    }

    @Override
    public List<Request> findByProperty(Property property) {
        List<Request> requests=new ArrayList<>();
        try {
            requests=requestDao.findByPropertyAndDeletedStatus(property,false);
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return requests;
    }

    @Override
    public List<Request> findByAgentId(String id) {
        List<Request> requests=new ArrayList<>();
        try {
            requests=requestDao.findByAgentUuidAndDeletedStatus(id,false);
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return requests;
    }

    @Override
    public Request formRequest(RequestController.InnerRequest request) {
        Request r=new Request();
        try{
            r.setClientEmail(request.getClientEmail());
            r.setAgentUuid(request.getAgentUuid());
            r.setClientUuid(request.getClientUuid());
            r.setERequestStatus(ERequestStatus.PENDING);
            r.setERequestType(ERequestType.valueOf(request.getRequestType()));
            r.setRequestComment(request.getComment());
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return r;
    }

    @Override
    public List<Request> findByPropertyAgent(String agent) {
        List<Request> requests=new ArrayList<>();
        try {
            requests=requestDao.findByPropertyAgent(agent);
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return requests;
    }

    @Override
    public List<Request> findByClientUuid(String uuid) {
        List<Request> requests=new ArrayList<>();
        try {
            requests=requestDao.findByClientUuid(uuid);
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return requests;
    }

    @Override
    public List<Request> findByClientUuidAndERequestStatusAndDoneAt(String client, String requestStatus, Date dt) {
        List<Request> requests=new ArrayList<>();
        try {
            requests=requestDao.findByClientUuidAndERequestStatusAndDoneAt(client,requestStatus,dt);
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return requests;
    }

    @Override
    public List<Request> findByAgentUuidAndERequestStatusAndDoneAt(String client, String requestStatus, Date dt) {
        List<Request> requests=new ArrayList<>();
        try {
            requests=requestDao.findByAgentUuidAndERequestStatusAndDoneAt(client,requestStatus,dt);
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return requests;
    }

    @Override
    public byte[] request(List<Request> r) {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        try {

            Document document = new Document(PageSize.A4);

            PdfWriter writer = PdfWriter.getInstance(document, bos);
            document.open();

            // ----------------Table Header "Title"----------------
            // font
            Font font = new Font(Font.FontFamily.HELVETICA, 14, Font.BOLD, BaseColor.BLACK);
            Font font2 = new Font(Font.FontFamily.HELVETICA, 9, Font.NORMAL, BaseColor.BLACK);
            document.add(new Paragraph(" "));
            Chunk title = new Chunk("   PPSS -Requests List ", font);
            Phrase phrase = new Phrase();
            phrase.add(title);
            Paragraph para = new Paragraph();
            para.add(phrase);
            para.setAlignment(Element.ALIGN_CENTER);
            document.add(para);
            document.add(new Paragraph(" "));

            PdfPTable table0 = new PdfPTable(3);
            PdfPTable tableList = new PdfPTable(1);
//            tableList.addCell();

            //Writing Printing Date Created
            Paragraph para2 = new Paragraph();
            para2.setAlignment(Element.ALIGN_LEFT);
            Phrase datePhrase = new Phrase();
            datePhrase.add(new Chunk("       Date Printed: " ,new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD, BaseColor.BLACK)));
            para2.add(datePhrase);
            document.add(para2);



            document.add(new Phrase("\n"));



            document.add(new Paragraph(""));
            document.add(table0);
            document.add(new Paragraph(" "));

            document.close();

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return bos.toByteArray();
    }

    @Override
    public byte[] propertyDetails(Request r) {
        return new byte[0];
    }

    // create cells
    private static PdfPCell createLabelCellDetails(String text) {
        // font
        Font font = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD, BaseColor.DARK_GRAY);

        // create cell
        PdfPCell cell = new PdfPCell(new Phrase(text, font));

        // set style
        // Style.labelCellStyle(cell);
        // cell.setBorder(PdfPCell.NO_BORDER);rgb()
        cell.setBackgroundColor(new BaseColor(237, 240, 244));
        cell.setPadding(new Float(6));
        return cell;
    }



    // create cells
    private static PdfPCell  praCell(Paragraph p) {
        PdfPCell cell = new PdfPCell(p);
        cell.setPadding(new Float(6));
        return cell;
    }
    // create cells
    private static PdfPCell createLabelCell(String text) {
        // font
        Font font = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD, BaseColor.DARK_GRAY);

        // create cell
        PdfPCell cell = new PdfPCell(new Phrase(text, font));

        // set style
        // Style.labelCellStyle(cell);
        // cell.setBorder(PdfPCell.NO_BORDER);rgb()

        // cell.setBackgroundColor(new BaseColor(237, 240, 244));
        cell.setPadding(new Float(6));
        return cell;
    }




    private static PdfPCell createCell(String text) {
        // font
        Font font = new Font(Font.FontFamily.HELVETICA, 5, Font.BOLD, BaseColor.DARK_GRAY);

        // create cell
        PdfPCell cell = new PdfPCell(new Phrase(text, font));

        // Setting Cell Height
        cell.setFixedHeight(10f);
        // set style
        // Style.labelCellStyle(cell);
        // cell.setBorder(PdfPCell.NO_BORDER);rgb()

        // cell.setBackgroundColor(new BaseColor(237, 240, 244));
        // cell.setPadding(new Float(6));
        return cell;
    }

    // create cells
    private static PdfPCell createValueCell(String text) {
        // font
        Font font = new Font(Font.FontFamily.HELVETICA, 8, Font.NORMAL, BaseColor.BLACK);

        // create cell
        PdfPCell cell = new PdfPCell(new Phrase(text, font));
        cell.setPadding(new Float(6));
        return cell;

    }

}
