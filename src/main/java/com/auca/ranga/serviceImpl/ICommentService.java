package com.auca.ranga.serviceImpl;

import com.auca.ranga.dao.CommentDao;
import com.auca.ranga.domain.Comment;
import com.auca.ranga.domain.Property;
import com.auca.ranga.service.CommentService;
import com.auca.ranga.utility.Messages;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Transactional
@Service
public class ICommentService implements CommentService {
    @Autowired
    private CommentDao commentDao ;

    @Override
    public String createOrUpdate(Comment comment) {
        try{
            commentDao.save(comment);
            return Messages.save;
        }catch (Exception ex){
            ex.printStackTrace();
            return Messages.error;
        }
    }
    @Override
    public List<Comment> findByProperty(Property property) {
        List<Comment> comments=new ArrayList<>();
        try {
            comments=commentDao.findByPropertyAndDeletedStatusOrderByDoneAtDesc(property,false);
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return comments;
    }
}
