package com.auca.ranga.utility;

import com.auca.ranga.domain.Property;
import com.auca.ranga.service.PropertyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.List;

@Component
public class Scheduler {
     @Autowired
    private PropertyService propertyService;
    /**
     * Schedule that will every day at 10:19 and at 59 second
     */

    @Scheduled(cron = "59 19 10 ? * * ")
    public void unpublishProperty() {
        List<Property> ps=propertyService.findByDeletedStatusAndAllowedToPublishAndAvailable(false,true,true);
        LocalDate d=LocalDate.now();
        for(Property p: ps){
            if(p.getRemoveDate().getDayOfMonth()==d.getDayOfMonth()){
                p.setAllowedToPublish(false);
               propertyService.update(p);
            }
        }

        System.out.println("Schuduler");
    }




}
