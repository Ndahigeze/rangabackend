package com.auca.ranga.utility;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.auca.ranga.dao.SubscriptionDao;
import com.auca.ranga.domain.PaymentObject;
import com.auca.ranga.domain.SubCategory;
import com.auca.ranga.domain.Subscription;
import com.braintreegateway.*;
import org.springframework.beans.factory.annotation.Autowired;

public class BrainTreeConfiguration {

    @Autowired
    private static SubscriptionDao dao;


    private static final String MERCHANT_ID ="p3xgfwy8ggwhhh7r";
 private static final String PUBLIC_KEY="b55kw4bv4d2gn5br";
 private static final String PRIVATE_KEY="d1060804be258c4dbf7b729522d787e5";

    /**
     * Generating client token
     * @return
     */
    public static String brainTreeAnvironment(){
         final BraintreeGateway gateway= new BraintreeGateway(Environment.SANDBOX, MERCHANT_ID,
                PUBLIC_KEY, PRIVATE_KEY);
                ClientTokenRequest clientTokenRequest = new ClientTokenRequest();
                    String clientToken = gateway.clientToken().generate(clientTokenRequest);
                return clientToken;
    }

    public static ResponceObject createTransaction(PaymentObject po,double amount){
        final BraintreeGateway gateway = new BraintreeGateway(Environment.SANDBOX, MERCHANT_ID, PUBLIC_KEY,PRIVATE_KEY);

        TransactionRequest request = new TransactionRequest()
                .amount(new BigDecimal(amount))
                .paymentMethodNonce(po.getClientNonce())
                .merchantAccountId("ranga_property_publish")
                .customer()
                .firstName(po.getClientName())
                .lastName("")
                .phone(po.getClientPhone())
                .email(po.getClientEmail())
                .customerId(po.getUuid())
                .done()
                .options()
                .submitForSettlement(true)
                .done();

        Result<Transaction> result = gateway.transaction().sale(request);
        ResponceObject r=new ResponceObject();
        if(result.isSuccess()){
          r.code=Messages.SUCCESS_CODE;
          r.object=result;
        }else{
          r.code=Messages.ERROR_CODE;
          r.object=result;
        }
        return r;
    }



    public static List<Transaction> findAllTransactions(){
      ///  System.out.println("test 64654654");
        final BraintreeGateway gateway = new BraintreeGateway(Environment.SANDBOX, MERCHANT_ID, PUBLIC_KEY,PRIVATE_KEY);
        TransactionSearchRequest request = new TransactionSearchRequest()
                .type().is(Transaction.Type.SALE);

        ResourceCollection<Transaction> collection = gateway.transaction().search(request);
        List<Transaction> transactions=new ArrayList<>();
        collection.iterator().forEachRemaining(transactions::add);
        return transactions;
    }

    public static class ResponceObject{
        private int code;
        private Object object;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public Object getObject() {
            return object;
        }

        public void setObject(Object object) {
            this.object = object;
        }
    }
}