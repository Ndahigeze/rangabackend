package com.auca.ranga.utility;

import javax.persistence.ManyToOne;


public class SMSProperty {

	 private String messageId;
	 private String cost;
	 private String status;
	 private String number;
	 

	public String getMessageId() {
		return messageId;
	}
	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}
	public String getCost() {
		return cost;
	}
	public void setCost(String cost) {
		this.cost = cost;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	 
	 
	 

}
