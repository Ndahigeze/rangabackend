package com.auca.ranga.utility;

import org.json.JSONArray;
import org.json.JSONObject;

public class SMS {


    /**
     * Function to send SMS
     *
     * @param recipients the recipients
     * @param message    the message
     * @return the sm s property
     */
    public static SMSProperty sendSingleSMS(String recipients, String message) {
        SMSProperty property = null;
        String username = "yesse";
        String apiKey = "9b88035e24e74449214da672a5c259c1dd2c9418f1dd88809ca863a4f61c7917";
        String from = "PPSS195590";
        AfricasTalkingGateway gateway = new AfricasTalkingGateway(username, apiKey);
        try {
            JSONArray results = gateway.sendMessage(recipients, message, from);
            for (int i = 0; i < results.length(); ++i) {
                property = new SMSProperty();
                JSONObject result = results.getJSONObject(i);
                property.setStatus(result.getString("status"));
                property.setNumber(result.getString("number"));
                property.setMessageId(result.getString("messageId"));
                property.setCost(result.getString("cost"));
            }
        } catch (Exception e) {
            System.out.print(e.getMessage());
            property = null;
        }
        return property;
    }

    // public static void main(String ...args){

    // 	try{
    // 		SMSProperty p=sendSingleSMS("250783382866", "Testing bfw");
    // 		System.out.println(p.getCost());

    // 	}catch (Exception e) {

    // 		System.out.println(e.getMessage());
    // 	}

    // }

}
