package com.auca.ranga;

import org.hibernate.SessionFactory;
import org.hibernate.jpa.HibernateEntityManagerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;


@Configuration
@SpringBootApplication
@EnableJpaAuditing
public class RangaApplication extends SpringBootServletInitializer {

	private static final Logger logger = LoggerFactory.getLogger(RangaApplication.class);
	public static void main(String[] args) {
		SpringApplication.run(RangaApplication.class, args);
		logger.info("--Application Started--"); 
	}
	
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(RangaApplication.class);
	}
	
	
	@Bean
	public SessionFactory sessionFactory(HibernateEntityManagerFactory h){
		return h.getSessionFactory();
		
	}

}

