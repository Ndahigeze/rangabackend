package com.auca.ranga.dao;

import com.auca.ranga.domain.Property;
import com.auca.ranga.domain.Suggestion;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface SuggestionDao extends JpaRepository<Suggestion,Long> {

    List<Suggestion> findByPropertyAndDeletedStatus(Property property,boolean ds);
}
