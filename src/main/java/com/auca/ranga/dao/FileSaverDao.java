package com.auca.ranga.dao;

import com.auca.ranga.domain.FileSaver;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FileSaverDao extends JpaRepository<FileSaver,Long> {

    FileSaver findByUuidAndDeletedStatus(String uuid,boolean ds);
     List<FileSaver> findByRefenceNameAndReferenceUuid(String name,String uuid);
}
