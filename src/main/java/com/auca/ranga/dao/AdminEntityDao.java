package com.auca.ranga.dao;


import org.springframework.data.jpa.repository.JpaRepository;
import com.auca.ranga.domain.*;

public interface AdminEntityDao extends JpaRepository<AdminEntity, String> {
}