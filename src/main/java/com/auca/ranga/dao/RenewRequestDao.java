package com.auca.ranga.dao;

import java.util.List;

import com.auca.ranga.domain.RenewRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RenewRequestDao extends JpaRepository<RenewRequest,Long> {


    RenewRequest findByUuid(String uid);
    List<RenewRequest> findByRequestAgentUuid(String uid);
    List<RenewRequest> findByRequestClientUuid(String uid);
}
