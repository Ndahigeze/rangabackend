package com.auca.ranga.dao;

import com.auca.ranga.domain.ERequestStatus;
import com.auca.ranga.domain.Property;
import com.auca.ranga.domain.Request;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface RequestDao extends JpaRepository<Request,Long> {

    Request findByUuidAndDeletedStatus(String uuid,boolean ds);
    List<Request> findByPropertyAndClientUuidAndDeletedStatus(Property property,String uuid,boolean ds);
    List<Request> findByPropertyAndDeletedStatus(Property property,boolean ds);
    List<Request> findByAgentUuidAndDeletedStatus(String uuid,boolean ds);
    List<Request> findByPropertyAgent(String agent);
    List<Request> findByClientUuid(String uuid);
    List<Request> findByClientUuidAndERequestStatusAndDoneAt(String client, String requestStatus, Date dt);
    List<Request> findByAgentUuidAndERequestStatusAndDoneAt(String client, String requestStatus, Date dt);

}
