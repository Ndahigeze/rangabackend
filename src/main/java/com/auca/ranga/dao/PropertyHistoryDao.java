package com.auca.ranga.dao;

import com.auca.ranga.domain.Property;
import com.auca.ranga.domain.PropertyHistory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PropertyHistoryDao extends JpaRepository<PropertyHistory, Long> {

    public List<PropertyHistory> findByProperty(Property p);

}
