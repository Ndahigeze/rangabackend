package com.auca.ranga.dao;

import com.auca.ranga.domain.Comment;
import com.auca.ranga.domain.Property;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CommentDao extends JpaRepository<Comment,Long> {

    List<Comment> findByPropertyAndDeletedStatusOrderByDoneAtDesc(Property property,boolean ds);

}
