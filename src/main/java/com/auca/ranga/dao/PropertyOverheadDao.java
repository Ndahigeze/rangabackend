package com.auca.ranga.dao;

import com.auca.ranga.domain.Property;
import com.auca.ranga.domain.PropertyOverhead;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PropertyOverheadDao extends JpaRepository<PropertyOverhead,Long> {

    List<PropertyOverhead> findByPropertyAndDeletedStatus(Property property,boolean ds);
    PropertyOverhead findByUuidAndDeletedStatus(String uuid,boolean ds);

}
