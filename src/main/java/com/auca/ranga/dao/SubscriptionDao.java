package com.auca.ranga.dao;

import com.auca.ranga.domain.SubCategory;
import com.auca.ranga.domain.Subscription;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SubscriptionDao extends JpaRepository<Subscription,Long> {

    public Subscription findByUuid(String uuid);
    public Subscription findByMonths(int id);
    public Subscription findByCategory(SubCategory c);

}
