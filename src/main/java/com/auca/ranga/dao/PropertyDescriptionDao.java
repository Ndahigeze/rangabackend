package com.auca.ranga.dao;

import com.auca.ranga.domain.PropertyDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PropertyDescriptionDao extends JpaRepository<PropertyDetails,Long> {

    PropertyDetails findByUuidAndDeletedStatus(String uuid, boolean ds);

    PropertyDetails findByIdAndDeletedStatus(Long id, boolean ds);

    List<PropertyDetails> findByPropertyUuidAndDeletedStatus(String uuid, boolean ds);


}
