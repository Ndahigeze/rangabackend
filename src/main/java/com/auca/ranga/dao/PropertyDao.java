package com.auca.ranga.dao;

import com.auca.ranga.domain.Property;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PropertyDao extends JpaRepository<Property,Long> {

    Property findByIdAndDeletedStatus(long id,boolean ds);

    Property findByUuidAndDeletedStatus(String uuid,boolean ds);

    List<Property> findByDeletedStatus(Boolean dStatus);

    List<Property> findByOwnerAndDeletedStatus(String uuid, boolean ds);

    List<Property> findByAgentAndDeletedStatus(String publisher,boolean ds);

    Property findByPropertyNumberAndDeletedStatus(String pn,boolean ds);

    List<Property> findByDeletedStatusAndAddressContaining(boolean ds,String ad);

    List<Property> findByDeletedStatusAndAllowedToPublishAndAvailable(boolean ds,boolean ab,boolean a);

    List<Property> findByDeletedStatusAndAllowedToPublishAndAvailableOrderByRequestTimesDesc(boolean ds,boolean ab,boolean a);
    List<Property> findByAgentAndDeletedStatusAndAllowedToPublishAndAvailable(String uuid,boolean ds,boolean ab,boolean a);
}
