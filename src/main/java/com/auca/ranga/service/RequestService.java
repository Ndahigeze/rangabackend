package com.auca.ranga.service;

import com.auca.ranga.controller.RequestController;
import com.auca.ranga.domain.ERequestStatus;
import com.auca.ranga.domain.Property;
import com.auca.ranga.domain.Request;

import java.util.Date;
import java.util.List;

public interface RequestService {

     int createOrUpdate(Request request);
     Request findOne(long id);
     Request findByUuid(String uuid);
     List<Request> findAll();
     List<Request> findByPropertyAndClientId(Property property,String id);
     List<Request> findByProperty(Property property);
     List<Request> findByAgentId(String id);
     Request formRequest(RequestController.InnerRequest request);
     List<Request> findByPropertyAgent(String agent);
     List<Request> findByClientUuid(String uuid);

    List<Request> findByClientUuidAndERequestStatusAndDoneAt(String client, String requestStatus, Date dt);
    List<Request> findByAgentUuidAndERequestStatusAndDoneAt(String client, String requestStatus, Date dt);
     byte[] request(List<Request> r);

     byte[] propertyDetails(Request r);
}
