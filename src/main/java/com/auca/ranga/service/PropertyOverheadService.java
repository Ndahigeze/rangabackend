package com.auca.ranga.service;

import com.auca.ranga.domain.Property;
import com.auca.ranga.domain.PropertyOverhead;

import java.util.List;

public interface PropertyOverheadService {

    String createOrUpdate(PropertyOverhead propertyOverhead);
    String delete(PropertyOverhead propertyOverhead);
    PropertyOverhead findOne(long id);
    PropertyOverhead findByUuid(String uuid);
    List<PropertyOverhead> findByProperty(Property property);
}
