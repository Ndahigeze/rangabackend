package com.auca.ranga.service;


import com.auca.ranga.domain.Message;

public interface MessageService {

    String create(Message msg);
    String update(Message msg);
    String delete(Message msg);
    String findOne(long id);
    String findByUuid(String uuid);
}
