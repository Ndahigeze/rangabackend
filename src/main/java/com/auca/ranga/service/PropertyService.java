package com.auca.ranga.service;

import com.auca.ranga.domain.Property;

import java.util.List;

public interface PropertyService {

    String create(Property pr);
    String update(Property pr);
    String delete(Property pr);
    Property  findOne(long id);
    Property  findByUuid(String uuid);
    Property findByPropertyNumber(String pn);
    List<Property> findByAddress(String address);
    List<Property> findByAgent(String publisherUuid);
    List<Property>  findByOwner(String ownerName);
    List<Property> findAll();
    List<Property> findByDeletedStatusAndAllowedToPublishAndAvailable(boolean ds,boolean ab,boolean a);
    List<Property> findbyUserAndAllowedToPublish(String uuid);
    List<Property> findByRequestTimes();

//    byte[] properties(List<Property> p);
//
//    byte[] propertyDetails(Property p);

}
