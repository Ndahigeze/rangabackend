package com.auca.ranga.service;

import com.auca.ranga.domain.FileSaver;

import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface FileSaverService {

    int createOrUpdate(FileSaver atachment);
    FileSaver findOne(long id);
    FileSaver findByUuid(String uuid);
    List<FileSaver> findByRefenceNameAndReferenceUuid(String name,String uuid);
    String saveProfile(MultipartFile file, String oldFile, String names) ;
    String saveRequestFile(MultipartFile file, String oldFile, String names);
}
