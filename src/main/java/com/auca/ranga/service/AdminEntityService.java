package com.auca.ranga.service;

import java.util.List;
import com.auca.ranga.domain.*;

public interface AdminEntityService {
    List<AdminEntity> findAll();
    AdminEntity findOne(String id);
}