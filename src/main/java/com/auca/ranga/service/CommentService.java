package com.auca.ranga.service;

import com.auca.ranga.domain.Comment;
import com.auca.ranga.domain.Property;

import java.util.List;

public interface CommentService {

    String createOrUpdate(Comment comment);
    List<Comment> findByProperty(Property property);

}
