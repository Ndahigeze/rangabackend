package com.auca.ranga.service;

import com.auca.ranga.domain.Property;
import com.auca.ranga.domain.Suggestion;

import java.util.List;

public interface SuggestionService {

    String createOrUpdate(Suggestion suggestion);
    String delete(Suggestion suggestion);
    List<Suggestion> findByProperty(Property property);
}
