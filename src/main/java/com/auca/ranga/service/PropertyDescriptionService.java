package com.auca.ranga.service;

import com.auca.ranga.domain.PropertyDetails;

import java.util.List;

public interface PropertyDescriptionService {

    String createOrUpdate(PropertyDetails description);
    PropertyDetails findOne(long id);
    PropertyDetails findByUuid(String uuid);
    List<PropertyDetails> findByProperty(String uuid);
}
