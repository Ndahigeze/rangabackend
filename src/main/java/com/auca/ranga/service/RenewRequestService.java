package com.auca.ranga.service;

import java.util.List;

import com.auca.ranga.domain.RenewRequest;

public interface RenewRequestService {

    int createOrUpdate(RenewRequest renewRequest);
    String delete(RenewRequest renewRequest);
    RenewRequest findOne(long id);
    RenewRequest findByUuid(String uuid);
    
    List<RenewRequest> findByRequestAgentUuid(String uid);

    List<RenewRequest> findByRequestClientUuid(String uid);

    List<RenewRequest> findAll();

    byte[] renewRequestReports(List<RenewRequest> r);

    byte[] renewRequestDetailReport(RenewRequest p);
}
